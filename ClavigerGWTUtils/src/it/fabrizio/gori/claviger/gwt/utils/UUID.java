package it.fabrizio.gori.claviger.gwt.utils;

import com.google.gwt.typedarrays.shared.Uint8Array;

public final class UUID {

	private static final char[] CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
	private static final char[] EXTENDED_CHARS = "-+*/.,;:<>!?^#@()[]0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();

	public static String generateRandom() {

		char[] result = new char[36];
		int r;

		result[8] = result[13] = result[18] = result[23] = '-';
		result[14] = '4';

		for (int i = 0; i < 36; i++) {

			if (result[i] == 0) {

				r = (int) (Math.random() * CHARS.length);
				result[i] = CHARS[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
			}
		}

		return new String(result).toLowerCase();
	}

	public static String generateSecureRandom() {
		
		int length = 16 + (int) (16 * Math.random());
		String result = "";
		
		Uint8Array idx = getRandomValues(length);

		for (int i = 0; i < length; i++) {

			result += EXTENDED_CHARS[idx.get(i) % EXTENDED_CHARS.length];
		}

		return result;
	};

	private static native Uint8Array getRandomValues(int length) /*-{
	
		var result;
		var crypto = window.crypto;
	
		if (!crypto) {
	
			result = [];
	
			for (i = 0; i < length; i++) {
	
				var seed = [
						((new Date()).getTime() * 41 ^ (Math.random() * Math
								.pow(2, 32))) >>> 0,
						((new Date()).getTime() * 41 ^ (Math.random() * Math
								.pow(2, 32))) >>> 0,
						((new Date()).getTime() * 41 ^ (Math.random() * Math
								.pow(2, 32))) >>> 0,
						((new Date()).getTime() * 41 ^ (Math.random() * Math
								.pow(2, 32))) >>> 0 ];
	
				result[i] = Math.abs((Math.floor(Math.random()
						* Math.pow(2, 32)) ^ (seed[i % 4] >>> (i % 23))));
			}
	
		} else {
	
			result = new Uint8Array(length);
	
			crypto.getRandomValues(result);
		}
	
		return result;
	}-*/;
}