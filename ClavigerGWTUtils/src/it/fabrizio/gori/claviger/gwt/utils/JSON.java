package it.fabrizio.gori.claviger.gwt.utils;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL)
public final class JSON {

	@JsMethod
	public native static <T> String stringify(T obj);

	@JsMethod
	public native static <T> T parse(String obj);
}