package it.fabrizio.gori.claviger.core;

import it.fabrizio.gori.claviger.core.provider.exceptions.UnauthenticatedException;
import it.fabrizio.gori.claviger.core.view.View;

public abstract class Application {

	private ViewFactory viewFactory;
	private ProviderFactory providerFactory;
	private Cipher cipher;
	private View<?> curentView = null;

	public final ViewFactory getViewFactory() {

		if (viewFactory == null) {

			viewFactory = createViewFactory();
		}

		return viewFactory;
	}

	public final ProviderFactory getProviderFactory() {

		if (providerFactory == null) {

			providerFactory = createProviderFactory();
		}

		return providerFactory;
	}

	public final Cipher getCipher() {

		if (cipher == null) {

			cipher = createChiper();
		}

		return cipher;
	}

	public View<?> getCurrentView() {

		return curentView;
	}

	public void setCurrentView(View<?> view) {

		this.curentView = view;
	}

	public final String getVersion() {

		return "v1.0";
	}

	public final void start() {

		getProviderFactory().getAuthorizationProvider().get()
				.then(a -> {

					getViewFactory().createDataView(a).open();

					return null;

				}).thenCatch(UnauthenticatedException.class, ex -> {

					getViewFactory().createLoginView().open();

					return null;

				}).thenCatch(ex -> {

					getViewFactory().createInfoView().open("Claviger",
							"Errore recupero autorizzazione (" + ex.getMessage() + ")")
							.whenClose(getViewFactory().createLoginView()::open);

					return null;
				});
	}

	abstract protected ViewFactory createViewFactory();

	abstract protected ProviderFactory createProviderFactory();

	abstract protected Cipher createChiper();
}