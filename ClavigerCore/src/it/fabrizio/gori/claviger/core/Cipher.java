package it.fabrizio.gori.claviger.core;

public interface Cipher {

	public String encrypt(String value, String password);

	public String decrypt(String value, String password);

	public String hash(String value);
}