package it.fabrizio.gori.claviger.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Promise<S> {

	private enum STATE {

		FULFILLED,
		REJECTED,
		PENDING;
	}

	@FunctionalInterface
	public static interface Supplier<T> {

		public T get() throws Throwable;
	}

	@FunctionalInterface
	public static interface Function<P, R> {

		public R apply(P a) throws Throwable;
	}

	@FunctionalInterface
	public static interface ResolverFn<S> {

		public void accept(Resolver<S> resolver, Rejecter rejecter) throws Throwable;
	}

	@FunctionalInterface
	public static interface FinishHandler {

		public void onFinish();
	}

	public static final class FirstFulfilledException extends Exception {

		private static final long serialVersionUID = 1L;

		private final List<Throwable> errors;

		private FirstFulfilledException(List<Throwable> errors) {

			this.errors = errors;
		}

		public List<Throwable> getErrors() {

			return errors;
		}

		public Optional<Throwable> ifContains(Class<?> clazz) {

			return errors.stream()
					.filter(i -> i != null && i.getClass().equals(clazz))
					.findFirst();
		}

		@Override
		public String getMessage() {

			return errors.stream()
					.map(Throwable::getMessage)
					.distinct()
					.collect(Collectors.joining(", "));
		}
	}

	public static final class Resolver<S> {

		private final Promise<S> promise;

		private Resolver(Promise<S> promise) {

			this.promise = promise;
		}

		public void accept(S result) {

			synchronized (promise) {

				if (promise.state == STATE.PENDING) {

					promise.result = result;
					promise.state = STATE.FULFILLED;

					if (promise.finishHandler != null) {

						promise.finishHandler.onFinish();
						promise.finishHandler = null;
					}
				}
			}
		}
	}

	public static final class Rejecter {

		private final Promise<?> promise;

		private Rejecter(Promise<?> promise) {

			this.promise = promise;
		}

		public void accept(Throwable t) {

			synchronized (promise) {

				if (promise.state == STATE.PENDING) {

					promise.rejectReason = t;
					promise.state = STATE.REJECTED;

					if (promise.finishHandler != null) {

						promise.finishHandler.onFinish();
						promise.finishHandler = null;
					}
				}
			}
		}
	}

	private FinishHandler finishHandler = null;
	private STATE state = STATE.PENDING;
	private S result;
	private Throwable rejectReason;

	public Promise(ResolverFn<S> fn) {

		Resolver<S> resolve = new Resolver<S>(this);
		Rejecter reject = new Rejecter(this);

		try {

			fn.accept(resolve, reject);

		} catch (Throwable e) {

			reject.accept(e);
		}
	}

	public <R> Promise<R> then(Function<? super S, Promise<? extends R>> fn) {

		return thenAlways(() -> {

			if (state == STATE.FULFILLED) {

				return fn != null ? fn.apply(result) : null;

			} else {

				throw rejectReason;
			}
		});
	}

	public <E extends Throwable> Promise<S> thenCatch(Function<? super E, Promise<? extends S>> fn) {

		return thenCatch(null, fn, null);
	}

	@SuppressWarnings("unchecked")
	public <E extends Throwable> Promise<S> thenCatch(Class<? super E> clazz, Function<? super E, Promise<? extends S>> action) {

		return thenCatch(new Class[] { clazz }, action);
	}

	public <E extends Throwable> Promise<S> thenCatch(Class<? super E>[] clazz, Function<? super E, Promise<? extends S>> action) {

		return thenCatch(clazz, action, t -> c -> Arrays.asList(c).contains(t));
	}

	@SuppressWarnings("unchecked")
	public <E extends Throwable> Promise<S> thenCatchExclude(Class<? super E> clazz, Function<Throwable, Promise<? extends S>> action) {

		return thenCatchExclude(new Class[] { clazz }, action);
	}

	public <E extends Throwable> Promise<S> thenCatchExclude(Class<E>[] clazz, Function<? super Throwable, Promise<? extends S>> action) {

		return thenCatch(clazz, action, t -> c -> !Arrays.asList(c).contains(t));
	}

	@SuppressWarnings("unchecked")
	private <E extends Throwable> Promise<S> thenCatch(Class<? super E>[] clazz, Function<? super E, Promise<? extends S>> fn,
			Function<Class<?>, Function<Class<? super E>[], Boolean>> check) {

		return thenAlways(() -> {

			if (state == STATE.FULFILLED) {

				return Promise.resolve(result);

			} else {

				if (clazz == null || check == null || check.apply(rejectReason.getClass()).apply(clazz)) {

					return fn != null ? fn.apply((E) rejectReason) : null;

				} else {

					return Promise.reject(rejectReason);
				}
			}
		});
	}

	public <R> Promise<R> thenAlways(Supplier<Promise<? extends R>> supplier) {

		return new Promise<R>((resolve, reject) -> {

			setFinishHandler(() -> {

				try {

					if (supplier != null) {

						Promise<? extends R> promise = supplier.get();

						if (promise != null) {

							promise.setFinishHandler(() -> {

								if (promise.state == STATE.FULFILLED) {

									resolve.accept(promise.result);

								} else {

									reject.accept(promise.rejectReason);
								}
							});

						} else {

							resolve.accept(null);
						}

					} else {

						resolve.accept(null);
					}

				} catch (Throwable ex) {

					reject.accept(ex);
				}
			});
		});
	}

	public static <S> Promise<S> resolve(S value) {

		return new Promise<S>((resolve, reject) -> {

			resolve.accept(value);
		});
	}

	public static <S> Promise<S> reject(Throwable t) {

		return new Promise<S>((resolve, reject) -> {

			reject.accept(t);
		});
	}

	private synchronized void setFinishHandler(FinishHandler handler) {

		if (state == STATE.PENDING) {

			finishHandler = handler;

		} else {

			handler.onFinish();
		}
	}

	@SafeVarargs
	public static <S> Promise<S> race(Promise<S>... promises) {

		return race(Arrays.asList(promises));
	}

	public static <S> Promise<S> race(List<Promise<S>> promises) {

		return new Promise<S>((resolve, reject) -> {

			for (Promise<S> i : promises) {

				i.then(r -> {

					resolve.accept(r);

					return null;

				}).thenCatch(t -> {

					reject.accept(t);

					return null;
				});
			}
		});
	}

	@SafeVarargs
	public static <S> Promise<S> firstFulfilled(Promise<S>... promises) {

		return firstFulfilled(Arrays.asList(promises));
	}

	public static <S> Promise<S> firstFulfilled(List<Promise<S>> promises) {

		return new Promise<S>((resolve, reject) -> {

			List<Throwable> errors = new ArrayList<Throwable>();

			for (Promise<S> i : promises) {

				i.then(r -> {

					resolve.accept(r);

					return null;

				}).thenCatch(t -> {

					errors.add(t);

					if (errors.size() >= promises.size()) {

						reject.accept(new FirstFulfilledException(errors));
					}

					return null;

				});
			}
		});
	}

	@SafeVarargs
	public static <S> Promise<List<S>> all(Promise<S>... promises) {

		return all(Arrays.asList(promises));
	}

	public static <S> Promise<List<S>> all(List<Promise<S>> promises) {

		return new Promise<List<S>>((resolve, reject) -> {

			List<S> results = new ArrayList<S>();

			for (Promise<S> i : promises) {

				i.then(r -> {

					results.add(r);

					if (results.size() >= promises.size()) {

						resolve.accept(results);
					}

					return null;

				}).thenCatch(t -> {

					reject.accept(t);

					return null;

				});
			}
		});
	}
}