package it.fabrizio.gori.claviger.core.view;

import it.fabrizio.gori.claviger.core.presenter.Presenter;

public interface View<P extends Presenter<?>> {

	public P getPresenter();

	public void setPresenter(P presenter);

	public void show();

	public void setEnabled(boolean enabled);

	public default void busy() {

	}

	public default void ready() {

	}
}