package it.fabrizio.gori.claviger.core.view;

import java.util.List;

import it.fabrizio.gori.claviger.core.model.Account;
import it.fabrizio.gori.claviger.core.presenter.DataPresenter;

public interface DataView extends View<DataPresenter> {

	public void renderData(List<Account> data);

	public List<Account> getData();

	public void enableSave(boolean enabled);

	public void enableReset(boolean enabled);

	public void setTitle(String title);
}