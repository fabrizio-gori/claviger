package it.fabrizio.gori.claviger.core.view;

import it.fabrizio.gori.claviger.core.presenter.LoginPresenter;

public interface LoginView extends View<LoginPresenter> {

	public String getUsername();

	public String getPassword();

	public void showError(String errorMessage);
}