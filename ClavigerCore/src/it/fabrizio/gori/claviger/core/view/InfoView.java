package it.fabrizio.gori.claviger.core.view;

import it.fabrizio.gori.claviger.core.presenter.InfoPresenter;

public interface InfoView extends View<InfoPresenter> {

	public void setTitle(String title);

	public void setMessage(String message);

	public void hide();
}