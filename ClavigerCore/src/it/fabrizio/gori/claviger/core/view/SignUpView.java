package it.fabrizio.gori.claviger.core.view;

import it.fabrizio.gori.claviger.core.presenter.SignUpPresenter;

public interface SignUpView extends View<SignUpPresenter> {

	public String getUsername();

	public void setUsername(String username);

	public String getPassword();

	public void setPassword(String password);

	public String getPasswordConfirm();

	public void showError(String errorMessage);
}