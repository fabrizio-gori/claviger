package it.fabrizio.gori.claviger.core.view;

import it.fabrizio.gori.claviger.core.presenter.QuestionPresenter;

public interface QuestionView extends View<QuestionPresenter> {

	public void setTitle(String title);

	public void setMessage(String message);

	public void hide();
}