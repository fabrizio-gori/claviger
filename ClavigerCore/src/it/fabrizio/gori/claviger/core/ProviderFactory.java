package it.fabrizio.gori.claviger.core;

import it.fabrizio.gori.claviger.core.model.Account;
import it.fabrizio.gori.claviger.core.provider.AuthorizationProvider;
import it.fabrizio.gori.claviger.core.provider.DataProvider;

public interface ProviderFactory {

	public AuthorizationProvider getAuthorizationProvider();

	public DataProvider<Account> getAccountProvider();

	public String getVersion();
}