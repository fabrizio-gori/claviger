package it.fabrizio.gori.claviger.core.presenter;

import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.provider.AuthorizationProvider;
import it.fabrizio.gori.claviger.core.provider.exceptions.NotRegisteredException;
import it.fabrizio.gori.claviger.core.provider.exceptions.WrongPasswordException;
import it.fabrizio.gori.claviger.core.view.LoginView;

public final class LoginPresenter extends Presenter<LoginView> {

	private final AuthorizationProvider authorizationProvider;

	public LoginPresenter(Application app, LoginView view) {

		super(app, view);

		this.authorizationProvider = app.getProviderFactory().getAuthorizationProvider();
	}

	public void doLogin() {

		getView().busy();

		final String username = getView().getUsername(), password = getView().getPassword();

		authorizationProvider.authenticate(username, password)
				.then(auth -> {

					getApplication().getViewFactory().createDataView(auth).open();

					return null;

				}).thenCatch(NotRegisteredException.class, ex -> {

					getApplication().getViewFactory().createQuestionView().open("Claviger",
							"Utente \"" + username + "\" non registrato.\n\nSi vuole procedere con la creazione di un nuovo utente?")
							.whenYes(() -> getApplication().getViewFactory().createSignUpView().open(username, password))
							.whenNo(() -> {

								getView().showError("Utente \"" + username + "\"  non registrato.");
								getView().ready();
							});

					return null;

				}).thenCatch(WrongPasswordException.class, ex -> {

					getView().showError("Password errata.");
					getView().ready();

					return null;

				}).thenCatch(ex -> {

					getView().showError(ex.getMessage());
					getView().ready();

					return null;
				});
	}
}