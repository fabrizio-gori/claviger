package it.fabrizio.gori.claviger.core.presenter;

import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.view.QuestionView;

public final class QuestionPresenter extends Presenter<QuestionView> {

	public class NoAction {

		public void whenNo(Runnable noCallback) {

			QuestionPresenter.this.noCallback = noCallback;
		}
	}

	public final class Action extends NoAction {

		private Action() {

		}

		public NoAction whenYes(Runnable yesCallback) {

			QuestionPresenter.this.yesCallback = yesCallback;

			return this;
		}
	}

	private Runnable yesCallback = null;
	private Runnable noCallback = null;

	public QuestionPresenter(Application application, QuestionView view) {

		super(application, view);
	}

	public Action open(String title, String message) {

		getView().setTitle(title);
		getView().setMessage(message);
		getView().show();

		return new Action();
	}

	public void yes() {

		if (yesCallback != null) {

			yesCallback.run();
		}

		getView().hide();
	}

	public void no() {

		getView().hide();

		if (noCallback != null) {

			noCallback.run();
		}
	}
}