package it.fabrizio.gori.claviger.core.presenter;

import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.view.InfoView;

public final class InfoPresenter extends Presenter<InfoView> {

	public final class Action {

		private Action() {

		}

		public Action whenClose(Runnable closeCallback) {

			InfoPresenter.this.closeCallback = closeCallback;

			return this;
		}
	}

	private Runnable closeCallback = null;

	public InfoPresenter(Application application, InfoView view) {

		super(application, view);
	}

	public Action open(String title, String message) {

		getView().setTitle(title);
		getView().setMessage(message);
		getView().show();

		return new Action();
	}

	public void close() {

		getView().hide();

		if (closeCallback != null) {

			closeCallback.run();
		}
	}
}
