package it.fabrizio.gori.claviger.core.presenter;

import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.provider.AuthorizationProvider;
import it.fabrizio.gori.claviger.core.view.SignUpView;

public final class SignUpPresenter extends Presenter<SignUpView> {

	private final AuthorizationProvider authorizationProvider;

	public SignUpPresenter(Application application, SignUpView view) {

		super(application, view);

		authorizationProvider = application.getProviderFactory().getAuthorizationProvider();
	}

	public void open(String username, String password) {

		getView().setUsername(username);
		getView().setPassword(password);
		getView().show();
	}

	public final void doSignUp() {

		String password = getView().getPassword(), confirmPassword = getView().getPasswordConfirm();

		getView().busy();

		if (password == null || "".equals(password.trim())) {

			getView().showError("La password non può essere vuota.");
			getView().ready();

		} else if (confirmPassword == null || "".equals(confirmPassword.trim())) {

			getView().showError("Conferma password vuota.");
			getView().ready();

		} else if (!password.equals(confirmPassword)) {

			getView().showError("La conferma password non è corretta.");
			getView().ready();

		} else {

			authorizationProvider.signUp(getView().getUsername(), password)
					.then(auth -> {

						getApplication().getViewFactory().createDataView(auth).open();

						return null;

					}).thenCatch(ex -> {

						getView().showError(ex.getMessage());
						getView().ready();

						return null;
					});
		}

	}

	public void doBack() {

		getApplication().getViewFactory().createLoginView().open();
	}
}
