package it.fabrizio.gori.claviger.core.presenter;

import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.view.View;

@SuppressWarnings("rawtypes")
public abstract class Presenter<V extends View> {

	private final V view;
	private final Application application;

	@SuppressWarnings("unchecked")
	public Presenter(Application application, V view) {

		this.application = application;
		this.view = view;
		this.view.setPresenter(this);
	}

	protected final V getView() {

		return view;
	}

	protected final Application getApplication() {

		return application;
	}

	public final void open() {

		getView().show();

		getApplication().setCurrentView(getView());

		postOpen();
	}

	protected void postOpen() {

	}
}