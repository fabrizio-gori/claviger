package it.fabrizio.gori.claviger.core.presenter;

import java.util.Comparator;
import java.util.List;

import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.Promise;
import it.fabrizio.gori.claviger.core.model.Account;
import it.fabrizio.gori.claviger.core.model.Authorization;
import it.fabrizio.gori.claviger.core.provider.DataProvider;
import it.fabrizio.gori.claviger.core.provider.exceptions.ConcurrencyException;
import it.fabrizio.gori.claviger.core.provider.exceptions.UnauthorizedException;
import it.fabrizio.gori.claviger.core.view.DataView;

public final class DataPresenter extends Presenter<DataView> {

	private static final Comparator<Account> ACCOUNT_DESC_SORT = Comparator.comparing(Account::getDescription,
			Comparator.nullsFirst(Comparator.naturalOrder()));

	private final DataProvider<Account> accountProvider;
	private final Authorization authorization;

	private String concurrencyId = null;

	public DataPresenter(Application application, Authorization authorization, DataView view) {

		super(application, view);

		this.accountProvider = application.getProviderFactory().getAccountProvider();
		this.authorization = authorization;
	}

	@Override
	public void postOpen() {

		getView().setTitle(authorization.getUsername());

		doRetrieveData();
	}

	public void doRetrieveData() {

		getView().busy();

		accountProvider.getAll(authorization, ACCOUNT_DESC_SORT)
				.then(dataResult -> {

					concurrencyId = dataResult.getConcurrencyId();

					getView().renderData(dataResult.getData());

					getView().ready();

					return null;

				}).thenCatch(UnauthorizedException.class, this::onUnauthorized)
				.thenCatch(this::onError);
	}

	public void doSaveData() {

		getView().busy();

		final List<Account> data = getView().getData();

		accountProvider.save(authorization, concurrencyId, data)
				.<Void>then(newConcurrenyId -> accountProvider.getAll(authorization, ACCOUNT_DESC_SORT)
						.then(dataResult -> {

							concurrencyId = dataResult.getConcurrencyId();

							getView().renderData(dataResult.getData());

							getView().ready();

							return null;
						}))
				.thenCatch(UnauthorizedException.class, this::onUnauthorized)
				.thenCatch(ConcurrencyException.class, this::onConcurrenyViolation)
				.thenCatch(this::onError);
	}

	public void doLogout() {

		getView().busy();

		getApplication().getProviderFactory().getAuthorizationProvider().revoke(authorization)
				.then(v -> {

					getApplication().getViewFactory().createLoginView().open();

					return null;

				}).thenCatch(this::onError);
	}

	public void showInfo() {

		getView().setEnabled(false);

		getApplication().getViewFactory().createInfoView().open("Informazioni su Claviger",
				"Application:     \t" + getApplication().getVersion() + "\n" + "View Factory:    \t"
						+ getApplication().getViewFactory().getVersion() + "\n" + "Provider Factory:\t"
						+ getApplication().getProviderFactory().getVersion())
				.whenClose(() -> getView().setEnabled(true));
	}

	private <T> Promise<T> onError(Throwable t) {

		getApplication().getViewFactory().createInfoView().open("Claviger", t.getMessage())
				.whenClose(() -> getView().ready());

		return null;
	}

	private <T> Promise<T> onConcurrenyViolation(ConcurrencyException ex) {

		getApplication().getViewFactory().createInfoView().open("Errore di concorrenza",
				"I dati sono stati modificati da un altro trattamento e saranno ricaricati aggiornati.")
				.whenClose(this::doRetrieveData);

		return null;
	}

	private <T> Promise<T> onUnauthorized(UnauthorizedException ex) {

		getApplication().getViewFactory().createInfoView().open("Claviger",
				"Autorizzazione non valida. Effettuare nuovamente il login per accedere ai dati.")
				.whenClose(this::doLogout);

		return null;
	}
}