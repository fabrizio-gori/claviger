package it.fabrizio.gori.claviger.core.provider.exceptions;

public final class WrongPasswordException extends Exception {

	private static final long serialVersionUID = 1L;
}