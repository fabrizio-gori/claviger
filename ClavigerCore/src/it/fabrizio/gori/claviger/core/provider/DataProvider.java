package it.fabrizio.gori.claviger.core.provider;

import java.util.Comparator;
import java.util.List;

import it.fabrizio.gori.claviger.core.Promise;
import it.fabrizio.gori.claviger.core.model.Authorization;

public interface DataProvider<T> {

	public static final class DataResult<T> {

		public static final class Builder<T> {

			private DataResult<T> result = new DataResult<T>();

			private Builder() {

			}

			public Builder<T> setData(List<T> data) {

				result.data = data;

				return this;
			}

			public Builder<T> setConcurrencyId(String concurrencyId) {

				result.concurrencyId = concurrencyId;

				return this;
			}

			public DataResult<T> create() {

				return result;
			}
		}

		public static final <T> Builder<T> builder() {

			return new Builder<T>();
		}

		private List<T> data;
		private String concurrencyId;

		public List<T> getData() {

			return data;
		}

		public String getConcurrencyId() {

			return concurrencyId;
		}
	}

	public Promise<DataResult<T>> getAll(Authorization auth, Comparator<T> sortComparator);

	public Promise<String> save(Authorization auth, String concurrencyId, List<T> data);
}