package it.fabrizio.gori.claviger.core.provider;

import it.fabrizio.gori.claviger.core.Promise;
import it.fabrizio.gori.claviger.core.model.Authorization;

public interface AuthorizationProvider {

	public Promise<Authorization> get();

	public Promise<Void> revoke(Authorization authorization);

	public Promise<Authorization> authenticate(String username, String password);

	public Promise<Authorization> signUp(String username, String password);
}