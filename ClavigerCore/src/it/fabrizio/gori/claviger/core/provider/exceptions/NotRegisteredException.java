package it.fabrizio.gori.claviger.core.provider.exceptions;

public class NotRegisteredException extends Exception {

	private static final long serialVersionUID = 1L;

	public NotRegisteredException(String message) {

		super(message);
	}
}
