package it.fabrizio.gori.claviger.core.provider.exceptions;

public class UnauthenticatedException extends Exception {

	private static final long serialVersionUID = 1L;

	public UnauthenticatedException() {

		super("Unauthenticated");
	}
}
