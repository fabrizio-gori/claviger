package it.fabrizio.gori.claviger.core.provider.exceptions;

public final class UnauthorizedException extends Exception {

	private static final long serialVersionUID = 1L;

	public UnauthorizedException() {

		super("Unauthorized");
	}
}