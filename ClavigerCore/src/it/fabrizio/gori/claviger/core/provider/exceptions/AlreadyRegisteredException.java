package it.fabrizio.gori.claviger.core.provider.exceptions;

public class AlreadyRegisteredException extends Exception {

	private static final long serialVersionUID = 1L;

	public AlreadyRegisteredException(String message) {

		super(message);
	}
}
