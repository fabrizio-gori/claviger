package it.fabrizio.gori.claviger.core.provider.exceptions;

public final class ConcurrencyException extends Exception {

	private static final long serialVersionUID = 1L;

	public ConcurrencyException() {

		super("Concurrency Exception");
	}
}