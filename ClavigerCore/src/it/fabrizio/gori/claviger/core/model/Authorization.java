package it.fabrizio.gori.claviger.core.model;

import java.util.Objects;

public final class Authorization {

	private final String username;
	private final String password;

	public Authorization(String username, String password) {

		Objects.requireNonNull(username, "Impossibile creare autorizzazioni senza username");
		Objects.requireNonNull(password, "Impossibile creare autorizzazioni senza password");

		this.username = username;
		this.password = password;
	}

	public String getUsername() {

		return username;
	}

	public String getPassword() {

		return password;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Authorization other = (Authorization) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
}