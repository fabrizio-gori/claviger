package it.fabrizio.gori.claviger.core.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class Account {

	private final String description;
	private final List<String> infos = new ArrayList<String>();

	public Account(String description) {

		this.description = description;
	}

	public String getDescription() {

		return description;
	}

	public Collection<String> getInfos() {

		return infos;
	}
}