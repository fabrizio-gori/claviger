package it.fabrizio.gori.claviger.core;

import it.fabrizio.gori.claviger.core.model.Authorization;
import it.fabrizio.gori.claviger.core.presenter.DataPresenter;
import it.fabrizio.gori.claviger.core.presenter.InfoPresenter;
import it.fabrizio.gori.claviger.core.presenter.LoginPresenter;
import it.fabrizio.gori.claviger.core.presenter.QuestionPresenter;
import it.fabrizio.gori.claviger.core.presenter.SignUpPresenter;

public interface ViewFactory {

	public LoginPresenter createLoginView();

	public InfoPresenter createInfoView();

	public QuestionPresenter createQuestionView();

	public SignUpPresenter createSignUpView();

	public DataPresenter createDataView(Authorization authorization);

	public String getVersion();
}