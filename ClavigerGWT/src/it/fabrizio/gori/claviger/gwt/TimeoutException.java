package it.fabrizio.gori.claviger.gwt;

public class TimeoutException extends Exception {

	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {

		return "Timeout";
	}
}