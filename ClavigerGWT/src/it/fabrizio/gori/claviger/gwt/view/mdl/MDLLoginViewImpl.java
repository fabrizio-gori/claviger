package it.fabrizio.gori.claviger.gwt.view.mdl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

import it.fabrizio.gori.claviger.core.presenter.LoginPresenter;
import it.fabrizio.gori.claviger.core.view.LoginView;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLButton;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLErrorLabel;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLInput;

final class MDLLoginViewImpl extends MDLPageView<LoginPresenter> implements LoginView {

	@UiTemplate("xml/MDLLoginViewImpl.ui.xml")
	interface UiBinderImpl extends UiBinder<Widget, MDLLoginViewImpl> {
	}

	private static UiBinderImpl uiBinder = GWT.create(UiBinderImpl.class);

	@UiField
	protected MDLInput usernameInput;
	@UiField
	protected MDLInput passwordInput;
	@UiField
	protected MDLErrorLabel errorLabel;
	@UiField
	protected MDLButton loginButton;

	public MDLLoginViewImpl() {

		initWidget(uiBinder.createAndBindUi(this));

		loginButton.addClickHandler(e -> getPresenter().doLogin());

		usernameInput.addDomHandler(e -> {

			if (e.getNativeKeyCode() == KeyCodes.KEY_ENTER) {

				passwordInput.setFocus(true);
			}

		}, KeyDownEvent.getType());

		passwordInput.addDomHandler(e -> {

			if (e.getNativeKeyCode() == KeyCodes.KEY_ENTER) {

				getPresenter().doLogin();
			}

		}, KeyDownEvent.getType());
	}

	@Override
	public String getUsername() {

		return usernameInput.getValue();
	}

	@Override
	public String getPassword() {

		return passwordInput.getValue();
	}

	@Override
	public void showError(String errorMessage) {

		errorLabel.setVisible(true);
		errorLabel.setText(errorMessage);
	}

	@Override
	public void show() {

		RootPanel.get().clear();
		RootPanel.get().add(this);
	}

	@Override
	protected void setEnabledImpl(boolean enabled) {
	
		usernameInput.setEnabled(enabled);
		passwordInput.setEnabled(enabled);
		loginButton.setEnabled(enabled);
	}
}