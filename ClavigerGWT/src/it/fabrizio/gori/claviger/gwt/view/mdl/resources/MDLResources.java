package it.fabrizio.gori.claviger.gwt.view.mdl.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;

public interface MDLResources extends ClientBundle {

	static final MDLResources INSTANCE = GWT.create(MDLResources.class);

	public static interface MDLStyle extends CssResource {

		@ClassName("mdl-window")
		public String mdlWindow();

		@ClassName("mdl-window__content")
		public String mdlWindowContent();

		@ClassName("mdl-window__content__header")
		public String mdlWindowContentHeader();

		@ClassName("mdl-window__content__header-big")
		public String mdlWindowContentHeaderBig();

		@ClassName("mdl-window__content__header-small")
		public String mdlWindowContentHeaderSmall();

		@ClassName("mdl-window__content__body")
		public String mdlWindowContentBody();

		@ClassName("mdl-window__content__body-big-footer")
		public String mdlWindowContentBodyBigFooter();

		@ClassName("mdl-window__content__body-small-footer")
		public String mdlWindowContentBodySmallFooter();

		@ClassName("mdl-window__content__body-big-no-footer")
		public String mdlWindowContentBodyBigNoFooter();

		@ClassName("mdl-window__content__body-small-no-footer")
		public String mdlWindowContentBodySmallNoFooter();

		@ClassName("mdl-window__content__body-maximixed")
		public String mdlWindowContentBodyMaximixed();

		@ClassName("mdl-window__content__footer")
		public String mdlWindowContentFooter();

		@ClassName("no-transition")
		public String noTransition();
	}

	@Source("Style.gss")
	MDLStyle style();
}