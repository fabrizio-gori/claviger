package it.fabrizio.gori.claviger.gwt.view.mdl;

import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.ViewFactory;
import it.fabrizio.gori.claviger.core.model.Authorization;
import it.fabrizio.gori.claviger.core.presenter.DataPresenter;
import it.fabrizio.gori.claviger.core.presenter.InfoPresenter;
import it.fabrizio.gori.claviger.core.presenter.LoginPresenter;
import it.fabrizio.gori.claviger.core.presenter.QuestionPresenter;
import it.fabrizio.gori.claviger.core.presenter.SignUpPresenter;

public class MDLViewFactory implements ViewFactory {

	private final Application application;

	public MDLViewFactory(Application application) {

		this.application = application;
	}

	@Override
	public LoginPresenter createLoginView() {

		return new LoginPresenter(application, new MDLLoginViewImpl());
	}

	@Override
	public QuestionPresenter createQuestionView() {

		return new QuestionPresenter(application, new MDLQuestionViewImpl());
	}

	@Override
	public InfoPresenter createInfoView() {

		return new InfoPresenter(application, new MDLInfoViewImpl());
	}

	@Override
	public SignUpPresenter createSignUpView() {

		return new SignUpPresenter(application, new MDLSignUpViewImpl());
	}

	@Override
	public DataPresenter createDataView(Authorization authorization) {

		return new DataPresenter(application, authorization, new MDLDataViewImpl());
	}

	@Override
	public String getVersion() {

		return "MDL v1.0";
	}
}