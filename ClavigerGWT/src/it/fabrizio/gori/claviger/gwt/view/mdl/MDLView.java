package it.fabrizio.gori.claviger.gwt.view.mdl;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.StyleInjector;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;

import it.fabrizio.gori.claviger.core.presenter.Presenter;
import it.fabrizio.gori.claviger.core.view.View;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLComponentHandler;
import it.fabrizio.gori.claviger.gwt.view.mdl.resources.MDLResources;

abstract class MDLView<P extends Presenter<?>> extends Composite implements View<P> {

	private static Timer noTransitionTimer = new Timer() {

		@Override
		public void run() {

			Document.get().getBody().removeClassName(MDLResources.INSTANCE.style().noTransition());
		}
	};

	static {

		MDLResources.INSTANCE.style().ensureInjected();

		Window.addResizeHandler(e -> {

			noTransitionTimer.cancel();
			Document.get().getBody().addClassName(MDLResources.INSTANCE.style().noTransition());
			noTransitionTimer.schedule(50);
		});
	}

	private P presenter;

	public MDLView() {

		addAttachHandler(e -> {

			StyleInjector.flush();
			MDLComponentHandler.upgradeDom();
		});
	}

	@Override
	public P getPresenter() {

		return presenter;
	}

	@Override
	public void setPresenter(P presenter) {

		this.presenter = presenter;
	}
}
