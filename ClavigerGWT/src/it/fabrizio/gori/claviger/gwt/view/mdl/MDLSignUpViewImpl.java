package it.fabrizio.gori.claviger.gwt.view.mdl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

import it.fabrizio.gori.claviger.core.presenter.SignUpPresenter;
import it.fabrizio.gori.claviger.core.view.SignUpView;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLButton;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLButtonFab;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLErrorLabel;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLInput;

final class MDLSignUpViewImpl extends MDLPageView<SignUpPresenter> implements SignUpView {

	@UiTemplate("xml/MDLSignUpViewImpl.ui.xml")
	interface UiBinderImpl extends UiBinder<Widget, MDLSignUpViewImpl> {
	}

	private static UiBinderImpl uiBinder = GWT.create(UiBinderImpl.class);

	@UiField
	protected MDLButtonFab backButton;
	@UiField
	protected MDLInput usernameInput;
	@UiField
	protected MDLInput passwordInput;
	@UiField
	protected MDLInput passwordConfirmInput;
	@UiField
	protected MDLErrorLabel errorLabel;
	@UiField
	protected MDLButton signUpButton;

	public MDLSignUpViewImpl() {

		initWidget(uiBinder.createAndBindUi(this));

		backButton.addClickHandler(e -> getPresenter().doBack());

		signUpButton.addClickHandler(e -> getPresenter().doSignUp());

		usernameInput.addDomHandler(e -> {

			if (e.getNativeKeyCode() == KeyCodes.KEY_ENTER) {

				passwordInput.setFocus(true);
			}

		}, KeyDownEvent.getType());

		passwordInput.addDomHandler(e -> {

			if (e.getNativeKeyCode() == KeyCodes.KEY_ENTER) {

				passwordConfirmInput.setFocus(true);
			}

		}, KeyDownEvent.getType());

		passwordConfirmInput.addDomHandler(e -> {

			if (e.getNativeKeyCode() == KeyCodes.KEY_ENTER) {

				getPresenter().doSignUp();
			}

		}, KeyDownEvent.getType());
	}

	@Override
	public String getUsername() {

		return usernameInput.getValue();
	}

	@Override
	public void setUsername(String username) {

		usernameInput.setValue(username);
	}

	@Override
	public String getPassword() {

		return passwordInput.getValue();
	}

	@Override
	public void setPassword(String password) {

		passwordInput.setValue(password);
	}

	@Override
	public String getPasswordConfirm() {

		return passwordConfirmInput.getValue();
	}

	@Override
	public void showError(String errorMessage) {

		errorLabel.setVisible(true);
		errorLabel.setText(errorMessage);
	}

	@Override
	public void show() {

		RootPanel.get().clear();
		RootPanel.get().add(this);
	}

	@Override
	protected void setEnabledImpl(boolean enabled) {
	
		usernameInput.setEnabled(enabled);
		passwordInput.setEnabled(enabled);
		passwordConfirmInput.setEnabled(enabled);
		signUpButton.setEnabled(enabled);
		backButton.setEnabled(enabled);
	}
}