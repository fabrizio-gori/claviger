package it.fabrizio.gori.claviger.gwt.view.mdl;

import it.fabrizio.gori.claviger.core.presenter.Presenter;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLLoadingPanel;

public abstract class MDLPageView<P extends Presenter<?>> extends MDLView<P> {

	private int busyRequest = 0;
	private int disableRequest = 0;
	private final MDLLoadingPanel waitPanel = new MDLLoadingPanel();

	@Override
	public final void busy() {

		busyRequest++;

		updateWaitPanel();
	}

	@Override
	public final void ready() {

		busyRequest--;

		updateWaitPanel();
	}

	public final void setEnabled(boolean enabled) {

		if (enabled) {

			disableRequest--;

		} else {

			disableRequest++;
		}

		updateWaitPanel();
	}

	private void updateWaitPanel() {

		if (busyRequest <= 0 && disableRequest <= 0) {

			busyRequest = 0;
			disableRequest = 0;

			setEnabledImpl(true);
			waitPanel.getElement().removeFromParent();

		} else if (busyRequest > 0 && disableRequest <= 0) {

			disableRequest = 0;

			if (!waitPanel.getElement().hasParentElement()) {

				getElement().insertFirst(waitPanel.getElement());
			}

			setEnabledImpl(false);
			waitPanel.showWaiting(true);

		} else if (busyRequest <= 0 && disableRequest > 0) {

			busyRequest = 0;

			if (!waitPanel.getElement().hasParentElement()) {

				getElement().insertFirst(waitPanel.getElement());
			}

			setEnabledImpl(false);
			waitPanel.showWaiting(false);

		} else {

			if (!waitPanel.getElement().hasParentElement()) {

				getElement().insertFirst(waitPanel.getElement());
			}

			setEnabledImpl(false);
			waitPanel.showWaiting(true);
		}
	}

	protected abstract void setEnabledImpl(boolean enabled);
}