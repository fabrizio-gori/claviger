package it.fabrizio.gori.claviger.gwt.view.mdl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

import it.fabrizio.gori.claviger.core.presenter.QuestionPresenter;
import it.fabrizio.gori.claviger.core.view.QuestionView;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLButton;

final class MDLQuestionViewImpl extends MDLView<QuestionPresenter> implements QuestionView {

	@UiTemplate("xml/MDLQuestionViewImpl.ui.xml")
	protected interface UiBinderImpl extends UiBinder<Widget, MDLQuestionViewImpl> {
	}

	private static UiBinderImpl uiBinder = GWT.create(UiBinderImpl.class);

	@UiField
	protected Label titleLabel;
	@UiField
	protected Label messageLabel;
	@UiField
	protected MDLButton yesButton;
	@UiField
	protected MDLButton noButton;

	public MDLQuestionViewImpl() {

		initWidget(uiBinder.createAndBindUi(this));

		yesButton.addClickHandler(e -> getPresenter().yes());
		noButton.addClickHandler(e -> getPresenter().no());
	}

	@Override
	public void setEnabled(boolean enabled) {

		yesButton.setEnabled(enabled);
		noButton.setEnabled(enabled);
	}

	@Override
	public void show() {

		RootPanel.get().add(this);
	}

	@Override
	public void hide() {

		removeFromParent();
	}

	@Override
	public void setMessage(String message) {

		messageLabel.setText(message);
	}

	@Override
	public void setTitle(String title) {

		titleLabel.setText(title);
	}
}