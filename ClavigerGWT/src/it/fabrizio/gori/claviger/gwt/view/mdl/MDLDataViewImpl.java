package it.fabrizio.gori.claviger.gwt.view.mdl;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.AnchorElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.dom.client.ObjectElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

import it.fabrizio.gori.claviger.core.model.Account;
import it.fabrizio.gori.claviger.core.presenter.DataPresenter;
import it.fabrizio.gori.claviger.core.view.DataView;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLAppLayout;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLButtonFab;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLEditable;

final class MDLDataViewImpl extends MDLPageView<DataPresenter> implements DataView {

	@UiTemplate("xml/MDLDataViewImpl.ui.xml")
	interface UiBinderImpl extends UiBinder<Widget, MDLDataViewImpl> {
	}

	private static UiBinderImpl uiBinder = GWT.create(UiBinderImpl.class);

	@UiField
	protected Label titleLabel;
	@UiField
	protected MDLButtonFab menuButton;
	@UiField
	protected MDLAppLayout appLayout;
	@UiField
	protected MDLEditable dataPanel;
	@UiField
	protected Button logoutButton;
	@UiField
	protected MDLButtonFab saveButton;
	@UiField
	protected MDLButtonFab editButton;
	@UiField
	protected MDLButtonFab resetButton;
	@UiField
	protected Button infoButton;
	@UiField
	protected Button searchButton;

	private Widget openAccountData = null;
	private Widget openAccountInfosData = null;

	public MDLDataViewImpl() {

		initWidget(uiBinder.createAndBindUi(this));

		dataPanel.getElement().setId("account-data");

		saveButton.setVisible(false);

		menuButton.addClickHandler(e -> appLayout.showMenu());

		logoutButton.addClickHandler(e -> getPresenter().doLogout());

		infoButton.addClickHandler(e -> getPresenter().showInfo());

		dataPanel.addDomHandler(e -> {

			if (dataPanel.getElement().getPropertyBoolean("disabled")) {

				dataPanel.getElement().blur();
				e.preventDefault();
			}

		}, FocusEvent.getType());

		dataPanel.addDomHandler(e -> {

			if (!dataPanel.getElement().getPropertyBoolean("disabled") && e.getNativeKeyCode() == KeyCodes.KEY_TAB) {

				insertTab(dataPanel.getElement());
				e.preventDefault();
			}

		}, KeyDownEvent.getType());

		dataPanel.setConverter(rawData -> {

			if (rawData.matches("^\\s*data:text/html;base64,.*")) {

				return "\t<span style='display:inline-block;margin-top:0.3rem;margin-bottom:0.3rem;' contenteditable='false' class='embedded-obj' embedded-data='"
						+ rawData + "'>" + base64Decode(rawData.trim().replace("data:text/html;base64,", ""))
						+ "</span>\n";

			} else if (rawData.matches("^\\s*data:image/.*;base64,.*")) {

				return "\t<img alt='Content data' style='display:inline-block;margin-top:0.3rem;margin-bottom:0.3rem;' contenteditable='false' class='embedded-obj' embedded-data='"
						+ rawData + "' src='" + rawData + "'></img>\n";

			} else if (rawData.matches("^\\s*data:.*;base64,.*")) {

				return "\t<object style='display:inline-block;margin-top:0.3rem;margin-bottom:0.3rem;' contenteditable='false' class='embedded-obj' embedded-data='"
						+ rawData + "' style='width:100%;' data='" + rawData + "'></object>\n";

			} else {

				return null;
			}
		});

		resetButton.addClickHandler(e -> {

			getPresenter().doRetrieveData();
		});

		editButton.addClickHandler(e -> {

			saveButton.setVisible(true);
			editButton.setVisible(false);

			dataPanel.setVisible(false);
			dataPanel.removeStyleName("animated");
			dataPanel.setEditable(true);
			dataPanel.setVisible(true);
		});

		saveButton.addClickHandler(e -> {

			getPresenter().doSaveData();
		});

		saveButton.setVisible(false);
		editButton.setVisible(true);
	}

	private native void insertAndExecute(Element el, String html) /*-{

		var scripts = "";
		var cleanedHtml = html.replace(/<script[^>]*>([\s\S]*?)<\/script>/gi,
				function() {

					scripts += arguments[1] + "\n";
					return "";
				});

		el.innerHTML = cleanedHtml;

		var scriptElement = document.createElement('script');
		scriptElement.setAttribute('type', 'text/javascript');
		scriptElement.innerHTML = scripts;
		el.appendChild(scriptElement);
	}-*/;

	@Override
	public void renderData(List<Account> data) {

		int idx = 0;

		dataPanel.setEditable(false);

		dataPanel.clear();

		dataPanel.addStyleName("animated");

		for (Account a : data) {

			HTMLPanel accountData = new HTMLPanel("<p>" + a.getDescription() + "</p>");
			accountData.setStyleName("account");
			accountData.removeStyleName("account-info-open");
			accountData.getElement().setId("account-info--" + (idx++));

			dataPanel.add(accountData);

			HTMLPanel accountInfosData = new HTMLPanel("");
			accountInfosData.getElement().setId("account-infos-data--" + (idx++));
			accountInfosData.setStyleName("account-infos");
			accountInfosData.getElement().getStyle().setProperty("maxHeight", "0");

			accountData.add(accountInfosData);

			if (a.getInfos().isEmpty()) {

				accountData.addStyleName("account__no-data");

			} else {

				Event.sinkEvents(accountData.getElement().getFirstChildElement(), Event.ONCLICK);
				Event.setEventListener(accountData.getElement().getFirstChildElement(), evt -> clickAccountWidget(accountData, accountInfosData));

				for (String j : a.getInfos()) {

					HTMLPanel accountInfoRow = new HTMLPanel(ParagraphElement.TAG, "");

					accountInfoRow.setStyleName("account-info");

					for (String k : SafeHtmlUtils.htmlEscapeAllowEntities(j.replaceAll("&", "&amp;")).split("\t")) {

						accountInfoRow.add(new HTMLPanel(SpanElement.TAG, "\t"));

						HTMLPanel info = null;

						if (k == null || k == "") {

						} else if (k.matches("^(http:|https:|ftp:)(.)*")) {

							info = new HTMLPanel(AnchorElement.TAG, k);

							info.getElement().setAttribute("target", "_blank");
							info.getElement().setAttribute("href", k.trim());
							info.getElement().setAttribute("rel", "noopener");

							accountInfoRow.add(info);

						} else if (k.matches("^\\s*data:text/html;base64,.*")) {

							info = new HTMLPanel(SpanElement.TAG, "");

							info.setStyleName("embedded-obj");
							info.getElement().getStyle().setDisplay(Display.INLINE_BLOCK);
							info.getElement().setAttribute("contenteditable", "false");
							info.getElement().setAttribute("embedded-data", k);

							insertAndExecute(info.getElement(), base64Decode(k.trim().replace("data:text/html;base64,", "")));

							accountInfoRow.add(info);

						} else if (k.matches("^\\s*data:image/.*;base64,.*")) {

							info = new HTMLPanel(ImageElement.TAG, "");

							info.setStyleName("embedded-obj");
							info.getElement().getStyle().setDisplay(Display.INLINE_BLOCK);
							info.getElement().setAttribute("contenteditable", "false");
							info.getElement().setAttribute("embedded-data", k);
							info.getElement().setAttribute("src", k);

							accountInfoRow.add(info);

						} else if (k.matches("^\\s*data:.*;base64,.*")) {

							info = new HTMLPanel(ObjectElement.TAG, "");

							info.setStyleName("embedded-obj");
							info.getElement().getStyle().setDisplay(Display.INLINE_BLOCK);
							info.getElement().setAttribute("contenteditable", "false");
							info.getElement().setAttribute("embedded-data", k);
							info.getElement().setAttribute("data", k);

							accountInfoRow.add(info);

						} else {

							info = new HTMLPanel(SpanElement.TAG, k);

							info.setStyleName("auto-copy");

							Event.sinkEvents(info.getElement(), Event.ONCLICK);
							Event.setEventListener(info.getElement(), evt -> {

								if (!dataPanel.isEditable()) {

									selectAndCopyText(evt.getEventTarget().cast());
								}
							});
							accountInfoRow.add(info);
						}
					}

					accountInfosData.add(accountInfoRow);
				}
			}
		}

		saveButton.setVisible(false);
		editButton.setVisible(true);
	}

	@Override
	public void enableSave(boolean enabled) {

	}

	@Override
	public void enableReset(boolean enabled) {

	}

	@Override
	public void show() {

		RootPanel.get().clear();
		RootPanel.get().add(this);
	}

	@Override
	public void setTitle(String title) {

		titleLabel.setText(title);
	}

	@Override
	public List<Account> getData() {

		return listFromHTML(dataPanel.getElement());
	}

	@Override
	protected void setEnabledImpl(boolean enabled) {

		menuButton.setEnabled(enabled);
		logoutButton.setEnabled(enabled);
		infoButton.setEnabled(enabled);
		searchButton.setEnabled(enabled);

		editButton.setEnabled(enabled);
		saveButton.setEnabled(enabled);
		resetButton.setEnabled(enabled);

		dataPanel.getElement().setPropertyBoolean("disabled", !enabled);
	}

	private native static String base64Decode(String data) /*-{

		return window.atob(data);
	}-*/;

	private String stringFromHTML(Element el) {

		String result = "";
		String nodeName = el.getNodeName().toLowerCase();

		if (el.hasClassName("embedded-obj")) {

			return el.getAttribute("embedded-data");

		} else if (nodeName == "#text") {

			result = el.getInnerText();

		} else if (nodeName == "div" || nodeName == "p" || nodeName == "br") {

			result += "\n";
		}
		for (int i = 0; i < el.getChildCount(); i++) {

			result += stringFromHTML(el.getChild(i).cast());
		}

		if (nodeName == "div" || nodeName == "p" || nodeName == "br") {

			result = "\n" + result;
		}

		return result;
	}

	private List<Account> listFromHTML(Element el) {

		String stringData = stringFromHTML(el);

		List<Account> result = new ArrayList<Account>();

		if (stringData != null) {

			Account account = null;

			for (String i : stringData.replaceAll("\r", "").trim().split("\n")) {

				if (i != null && !"".equals(i.trim())) {

					if (i.matches("^\\S(.)*")) {

						account = new Account(i.toUpperCase().trim());
						result.add(account);

					} else if (account != null) {

						account.getInfos().add(i.substring(1));
					}
				}
			}
		}

		return result;
	}

	private void clickAccountWidget(Widget accountData, Widget accountInfosData) {

		if (accountData.getElement().hasClassName("account-info--open")) {

			accountData.removeStyleName("account-info--open");
			accountInfosData.getElement().getStyle().setProperty("maxHeight", "0");

			openAccountData = null;
			openAccountInfosData = null;

		} else {

			accountData.addStyleName("account-info--open");
			accountInfosData.getElement().getStyle().setProperty("maxHeight", accountInfosData.getElement().getScrollHeight() + "px");

			if (openAccountData != null) {

				openAccountData.removeStyleName("account-info--open");
			}

			if (openAccountInfosData != null) {

				openAccountInfosData.getElement().getStyle().setProperty("maxHeight", "0");
			}

			openAccountData = accountData;
			openAccountInfosData = accountInfosData;
		}
	}

	private native void selectAndCopyText(Element el) /*-{

		try {

			var doc = el.ownerDocument;
			var win = doc.defaultView || doc.parentWindow;

			if (doc.body.createTextRange) {

				var range = doc.body.createTextRange();
				range.moveToElementText(el);
				range.select();

			} else if (win.getSelection) {

				var selection = win.getSelection();
				var range = doc.createRange();
				range.selectNodeContents(el);
				selection.removeAllRanges();
				selection.addRange(range);
			}

			doc.execCommand("copy");

		} catch (e) {

			console.log(e);
		}
	}-*/;

	private native void insertTab(Element el) /*-{

		try {

			var doc = el.ownerDocument;
			var sel = doc.getSelection();
			var range = sel.getRangeAt(0);
			var i = sel.anchorNode;

			while (!i.getAttribute || i.getAttribute("contenteditable") == null) {

				i = i.parentNode;
			}

			if (i.getAttribute("contenteditable").toLowerCase() === "true") {

				range.deleteContents();

				var tabNode = doc.createTextNode("\u0009");
				range.insertNode(tabNode);

				range.setStartAfter(tabNode);
				range.setEndAfter(tabNode);
				sel.removeAllRanges();
				sel.addRange(range);
			}

		} catch (e) {

			console.log(e);
		}
	}-*/;
}