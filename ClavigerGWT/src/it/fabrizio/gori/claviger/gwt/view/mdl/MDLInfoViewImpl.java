package it.fabrizio.gori.claviger.gwt.view.mdl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

import it.fabrizio.gori.claviger.core.presenter.InfoPresenter;
import it.fabrizio.gori.claviger.core.view.InfoView;
import it.fabrizio.gori.claviger.gwt.ui.mdl.MDLButton;

final class MDLInfoViewImpl extends MDLView<InfoPresenter> implements InfoView {

	@UiTemplate("xml/MDLInfoViewImpl.ui.xml")
	protected interface UiBinderImpl extends UiBinder<Widget, MDLInfoViewImpl> {
	}

	private static UiBinderImpl uiBinder = GWT.create(UiBinderImpl.class);

	@UiField
	protected Label titleLabel;
	@UiField
	protected Label messageLabel;
	@UiField
	protected MDLButton okButton;

	public MDLInfoViewImpl() {

		initWidget(uiBinder.createAndBindUi(this));

		okButton.addClickHandler(e -> getPresenter().close());
	}

	@Override
	public void setEnabled(boolean enabled) {

		okButton.setEnabled(enabled);
	}

	@Override
	public void show() {

		RootPanel.get().add(this);
	}

	@Override
	public void hide() {

		removeFromParent();
	}

	@Override
	public void setMessage(String message) {

		messageLabel.setText(message);
	}

	@Override
	public void setTitle(String title) {

		titleLabel.setText(title);
	}
}