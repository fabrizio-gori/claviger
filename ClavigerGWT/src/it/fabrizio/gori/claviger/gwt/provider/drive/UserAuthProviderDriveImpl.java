package it.fabrizio.gori.claviger.gwt.provider.drive;

import java.util.Objects;

import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.Promise;
import it.fabrizio.gori.claviger.core.provider.exceptions.AlreadyRegisteredException;
import it.fabrizio.gori.claviger.core.provider.exceptions.NotRegisteredException;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIFile;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIUtil;
import it.fabrizio.gori.claviger.gwt.provider.UserAuthProvider;

public class UserAuthProviderDriveImpl extends AbstractDriveProvider implements UserAuthProvider {

	public UserAuthProviderDriveImpl(Application application) {

		super(application);
	}

	@Override
	public Promise<String> getHash(String username) {

		try {

			Objects.requireNonNull(username, "Username vuoto");

			return getGAPIInstance()
					.then(this::searchClavigerFolder)
					.then(clavigerFolder -> searchUserFolder(clavigerFolder, username))
					.then(userFolder -> Promise.resolve(userFolder.getAppProperties().getHash()));

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}

	@Override
	public Promise<String> getSessionToken(String username) {

		try {

			Objects.requireNonNull(username, "Username vuoto");

			return getGAPIInstance()
					.then(this::searchClavigerFolder)
					.then(clavigerFolder -> searchUserFolder(clavigerFolder, username))
					.then(userFolder -> Promise.resolve(userFolder.getAppProperties().getSessionToken()));

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}

	@Override
	public Promise<Void> create(String username, String hash, String sessionToken) {

		try {

			Objects.requireNonNull(username, "Username vuoto");
			Objects.requireNonNull(hash, "Hash vuoto");

			return getGAPIInstance()
					.then(this::searchClavigerFolder)
					.then(clavigerFolder -> searchUserFolder(clavigerFolder, username))
					.<Void>then(userFolder -> {

						throw new AlreadyRegisteredException("Utente " + username + " già registrato");

					}).thenCatch(NotRegisteredException.class, ex -> getGAPIInstance()
							.then(this::searchClavigerFolder)
							.then(clavigerFolder -> createUserFolder(clavigerFolder, username, hash, sessionToken))
							.then(null));

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}

	@Override
	public Promise<Void> updateSessionToken(String username, String sessionToken) {

		try {

			Objects.requireNonNull(username, "Username vuoto");

			return getGAPIInstance()
					.then(this::searchClavigerFolder)
					.then(clavigerFolder -> searchUserFolder(clavigerFolder, username))
					.then(userFolder -> updateUserFolder(userFolder, sessionToken))
					.then(null);

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}

	private Promise<GAPIFile<FolderProp>> createUserFolder(GAPIFile<?> parent, String username, String hash, String sessionToken) {

		return createTimeoutPromise(new Promise<GAPIFile<FolderProp>>((resolve, reject) -> {

			GAPIFile<FolderProp> userFolder = new GAPIFile<FolderProp>();

			userFolder.setName(username);
			userFolder.setMimeType(FOLDER_MIMETYPE);
			userFolder.setParent(parent.getId());

			userFolder.setAppProperties(new FolderProp());
			userFolder.getAppProperties().setHash(hash);
			userFolder.getAppProperties().setSessionToken(sessionToken);

			parent.gapi().getClient().getDrive().create(userFolder).then(resp -> {

				if (resp.getStatus() == 200) {

					resolve.accept(userFolder);

				} else {

					reject.accept(GAPIUtil.asError(resp.getResult()));
				}

				return null;
			}).thenCatch(err -> {

				reject.accept(GAPIUtil.asError(err));

				return null;
			});
		}));
	}

	private Promise<GAPIFile<FolderProp>> updateUserFolder(GAPIFile<FolderProp> userFolder, String sessionToken) {

		return createTimeoutPromise(new Promise<GAPIFile<FolderProp>>((resolve, reject) -> {

			userFolder.getAppProperties().setSessionToken(sessionToken);

			userFolder.gapi().getClient().getDrive().update(userFolder).then(resp -> {

				if (resp.getStatus() == 200) {

					resolve.accept(resp.getResult());

				} else {

					reject.accept(GAPIUtil.asError(resp.getResult()));
				}

				return null;

			}).thenCatch(err -> {

				reject.accept(GAPIUtil.asError(err));

				return null;
			});
		}));
	}
}