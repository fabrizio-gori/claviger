package it.fabrizio.gori.claviger.gwt.provider.drive;

import elemental2.core.Global;
import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.Promise;
import it.fabrizio.gori.claviger.core.provider.exceptions.ConcurrencyException;
import it.fabrizio.gori.claviger.core.provider.exceptions.NotRegisteredException;
import it.fabrizio.gori.claviger.core.provider.exceptions.UnauthorizedException;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIClient;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIClient.HTTPMethodValues;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIDrive;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIFile;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIPromise;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIResponse;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIUtil;
import it.fabrizio.gori.claviger.gwt.provider.UserDataProvider;
import it.fabrizio.gori.claviger.gwt.utils.UUID;
import jsinterop.base.JsPropertyMap;

public class UserDataProviderDriveImpl extends AbstractDriveProvider implements UserDataProvider {

	protected static final String CLAVIGER_DATA_MIMETYPE = "application/claviger.data";

	private final String filename;

	public UserDataProviderDriveImpl(Application application, String filename) {

		super(application);

		this.filename = filename;
	}

	@Override
	public Promise<Data> getData(String username, String hash) {

		UserDataProvider.Data.Builder builder = new UserDataProvider.Data.Builder();

		return getGAPIInstance()
				.then(this::searchClavigerFolder)
				.then(clavigerFolder -> searchUserFolder(clavigerFolder, username, hash))
				.then(this::searchUserFile)
				.then(userFile -> {

					builder.setConcurrencyId(userFile.getAppProperties().getConcurrencyId());

					return Promise.resolve(userFile);

				}).then(this::downloadData)
				.then(encData -> Promise.resolve(builder.setEncData(encData).create()))
				.thenCatch(GAPIFile.NotFoundException.class, ex -> Promise.resolve(builder.create()));
	}

	@Override
	public Promise<String> update(String username, String hash, String concurrencyId, String encData) {

		Promise<GAPIFile<FolderProp>> searchUserFolder = getGAPIInstance()
				.then(this::searchClavigerFolder)
				.then(clavigerFolder -> searchUserFolder(clavigerFolder, username, hash));

		return searchUserFolder
				.then(this::searchUserFile)
				.then(userFile -> checkConcurrency(userFile, concurrencyId))
				.thenCatch(GAPIFile.NotFoundException.class, ex -> searchUserFolder.then(userFolder -> createUserFile(userFolder, concurrencyId)))
				.then(userFile -> uploadData(userFile, encData, true))
				.then(userFile -> Promise.resolve(userFile.getAppProperties().getConcurrencyId()));
	}

	@Override
	public Promise<Void> refresh(String username, String hash, String concurrencyId, String encData) {

		Promise<GAPIFile<FolderProp>> searchUserFolder = getGAPIInstance()
				.then(this::searchClavigerFolder)
				.then(clavigerFolder -> searchUserFolder(clavigerFolder, username, hash));

		return searchUserFolder
				.then(this::searchUserFile)
				.thenCatch(GAPIFile.NotFoundException.class, ex -> searchUserFolder.then(userFolder -> createUserFile(userFolder, concurrencyId)))
				.then(userFile -> uploadData(userFile, encData, false))
				.then(null);
	}

	private Promise<GAPIFile<FolderProp>> searchUserFolder(GAPIFile<?> parent, String folderName, String hash) {

		return createTimeoutPromise(new Promise<GAPIFile<FolderProp>>((resolve, reject) -> {

			GAPIDrive.ListOptions opts = new GAPIDrive.ListOptions();

			opts.fields(GAPIFile.Fields.ID, GAPIFile.Fields.NAME, GAPIFile.Fields.APP_PROPERTIES);
			opts.searchQuery("name = '" + folderName + "' and mimeType = '" + FOLDER_MIMETYPE + "' and '" + parent.getId()
					+ "' in parents and trashed = false");

			parent.gapi().getClient().getDrive().list(opts).then(r -> {

				if (r.getStatus() == 200) {

					if (r.getResult().files().length > 1) {

						reject.accept(new Exception("Multiple file found."));

					} else if (r.getResult().files().length <= 0) {

						reject.accept(new NotRegisteredException("Utente " + folderName + " non registrato"));

					} else {

						GAPIFile<FolderProp> result = r.getResult().<FolderProp>files()[0];

						if (result.getAppProperties().getHash() != hash) {

							reject.accept(new UnauthorizedException());
						}

						resolve.accept(result);
					}

				} else {

					reject.accept(GAPIUtil.asError(r.getResult()));
				}

				return null;

			}).thenCatch(err -> {

				reject.accept(GAPIUtil.asError(err));

				return null;
			});

		}));
	}

	private Promise<GAPIFile<FileProp>> searchUserFile(GAPIFile<FolderProp> parent) {

		return createTimeoutPromise(new Promise<GAPIFile<FileProp>>((resolve, reject) -> {

			GAPIDrive.ListOptions opts = new GAPIDrive.ListOptions();

			opts.fields(GAPIFile.Fields.ID, GAPIFile.Fields.APP_PROPERTIES, GAPIFile.Fields.MIME_TYPE);
			opts.searchQuery("name = '" + filename + "' and mimeType = '" + CLAVIGER_DATA_MIMETYPE + "' and '" + parent.getId()
					+ "' in parents and trashed = false");

			parent.gapi().getClient().getDrive().list(opts).then(r -> {

				if (r.getStatus() == 200) {

					if (r.getResult().files().length > 1) {

						reject.accept(new Exception("Multiple file found."));

					} else if (r.getResult().files().length <= 0) {

						reject.accept(new GAPIFile.NotFoundException("File " + filename + " not found."));

					} else {

						resolve.accept(r.getResult().<FileProp>files()[0]);
					}

				} else {

					reject.accept(GAPIUtil.asError(r.getResult()));
				}

				return null;

			}).thenCatch(err -> {

				reject.accept(GAPIUtil.asError(err));

				return null;
			});

		}));
	}

	private Promise<GAPIFile<FileProp>> createUserFile(GAPIFile<FolderProp> parent, String concurrencyId) {

		return createTimeoutPromise(new Promise<GAPIFile<FileProp>>((resolve, reject) -> {

			GAPIFile<FileProp> userFile = new GAPIFile<FileProp>();

			userFile.setName(filename);
			userFile.setMimeType(CLAVIGER_DATA_MIMETYPE);
			userFile.setParent(parent.getId());

			userFile.setAppProperties(new FileProp());
			userFile.getAppProperties().setConcurrencyId(concurrencyId);

			parent.gapi().getClient().getDrive().create(userFile).then(resp -> {

				if (resp.getStatus() == 200) {

					resolve.accept(resp.getResult());

				} else {

					reject.accept(GAPIUtil.asError(resp.getResult()));
				}

				return null;
			}).thenCatch(err -> {

				reject.accept(GAPIUtil.asError(err));

				return null;
			});
		}));
	}

	private Promise<String> downloadData(GAPIFile<FileProp> file) {

		return createTimeoutPromise(new Promise<String>((resolve, reject) -> {

			GAPIClient.RequestOptions opts = new GAPIClient.RequestOptions();
			JsPropertyMap<?> param = JsPropertyMap.of("alt", "media");

			opts.path("https://www.googleapis.com/drive/v3/files/" + file.getId());
			opts.method(GAPIClient.HTTPMethodValues.GET);
			opts.params(param);

			GAPIPromise<GAPIResponse<String>> req = file.gapi().getClient().request(opts);

			req.then(r -> {

				if (r.getStatus() == 200) {

					resolve.accept(r.getBody());

				} else {

					reject.accept(GAPIUtil.asError(r.getResult()));
				}

				return null;

			}).thenCatch(err -> {

				reject.accept(GAPIUtil.asError(err));

				return null;
			});
		}));
	}

	private Promise<GAPIFile<FileProp>> uploadData(GAPIFile<FileProp> file, String data, boolean updateConcurrencyId) {

		return createTimeoutPromise(new Promise<GAPIFile<FileProp>>((resolve, reject) -> {

			final String boundary = "-------314159265358979323846";
			final String delimiter = "\r\n--" + boundary + "\r\n";
			final String closeDelimiter = "\r\n--" + boundary + "--";

			GAPIClient.RequestOptions opts = new GAPIClient.RequestOptions();

			if (updateConcurrencyId) {

				if (file.getAppProperties() == null) {

					file.setAppProperties(new FileProp());
				}

				file.getAppProperties().setConcurrencyId(UUID.generateRandom());
			}

			opts.path("https://www.googleapis.com/upload/drive/v3/files/" + file.getId() + "?uploadType=multipart&keepRevisionForever=true");
			opts.method(HTTPMethodValues.PATCH);
			opts.headers(JsPropertyMap.of("Content-Type", "multipart/mixed; boundary='" + boundary + "'"));
			opts.body(delimiter +
					"Content-Type: application/json\r\n\r\n" +
					Global.JSON.stringify(JsPropertyMap.of("mimeType", file.getMimeType(), "appProperties", file.getAppProperties())) +
					delimiter +
					"Content-Type: " + file.getMimeType() + "\r\n" +
					"\r\n" +
					data +
					closeDelimiter);

			file.gapi().getClient().request(opts).then(resp -> {

				if (resp.getStatus() == 200) {

					resolve.accept(file);

				} else {

					reject.accept(GAPIUtil.asError(resp.getResult()));
				}

				return null;

			}).thenCatch(err -> {

				reject.accept(GAPIUtil.asError(err));

				return null;
			});
		}));
	}

	private Promise<GAPIFile<FileProp>> checkConcurrency(GAPIFile<FileProp> file, String concurrencyId) {

		try {

			if (file.getAppProperties().getConcurrencyId() != concurrencyId) {

				throw new ConcurrencyException();
			}

			return Promise.resolve(file);

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}
}