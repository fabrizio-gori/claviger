package it.fabrizio.gori.claviger.gwt.provider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import it.fabrizio.gori.claviger.core.Cipher;
import it.fabrizio.gori.claviger.core.Promise;

class AbstractProvider<T> {

	private final Consumer<Throwable> logger;
	private final T mainResourceProvider;
	private final List<T> fallbackResourceProviders;
	private final Cipher cipher;

	@SafeVarargs
	public AbstractProvider(Cipher cipher, T mainResourceProvider, T... fallbackResourceProviders) {

		this(cipher, null, mainResourceProvider, fallbackResourceProviders);
	}

	@SafeVarargs
	public AbstractProvider(Cipher cipher, Consumer<Throwable> logger, T mainResourceProvider, T... fallbackResourceProviders) {

		this.cipher = cipher;
		this.logger = logger;
		this.mainResourceProvider = mainResourceProvider;
		this.fallbackResourceProviders = Arrays.asList(fallbackResourceProviders);
	}

	protected T getMainProvider() {

		return mainResourceProvider;
	}

	protected List<T> getFallbackProviders() {

		return fallbackResourceProviders;
	}

	protected final void log(Throwable t) {

		if (logger != null) {

			logger.accept(t);
		}
	}

	protected <P> List<Promise<P>> createPromiseForAll(Function<T, Promise<P>> fn) {

		List<Promise<P>> result = new ArrayList<Promise<P>>();

		result.add(fn.apply(getMainProvider()));

		getFallbackProviders().forEach(i -> result.add(fn.apply(i)
				.thenCatch(ex -> {

					log(ex);

					throw ex;

				})));

		return result;
	}

	protected <P> List<Promise<P>> createPromiseForFallbacks(Function<T, Promise<P>> fn) {

		List<Promise<P>> result = new ArrayList<Promise<P>>();

		getFallbackProviders().forEach(i -> result.add(fn.apply(i)
				.thenCatch(ex -> {

					log(ex);

					throw ex;

				})));

		return result;
	}

	protected String encrypt(String value, String password) throws Throwable {

		String result = null;

		if (value != null) {

			result = cipher.encrypt(value, password).toString();
		}

		return result;
	}

	protected String decrypt(String value, String password) throws Throwable {

		String result = null;

		if (value != null) {

			result = cipher.decrypt(value, password).toString();
		}

		return result;
	}

	protected String hash(String value) throws Throwable {

		String result = null;

		if (value != null) {

			result = cipher.hash(value);
		}

		return result;
	}
}