package it.fabrizio.gori.claviger.gwt.provider.idb;

import it.fabrizio.gori.claviger.core.Promise;
import it.fabrizio.gori.claviger.core.provider.exceptions.AlreadyRegisteredException;
import it.fabrizio.gori.claviger.core.provider.exceptions.NotRegisteredException;
import it.fabrizio.gori.claviger.gwt.provider.UserAuthProvider;

public class UserAuthProviderIDBImpl extends AbstractIDBProvider implements UserAuthProvider {

	public UserAuthProviderIDBImpl(String idbName) {

		super(idbName);
	}

	@Override
	public Promise<String> getHash(String username) {

		return getResource(username)
				.then(res -> {

					if (res == null) {

						throw new NotRegisteredException("Utente " + username + " non registrato");
					}

					return Promise.resolve(res.getHash());
				});
	}

	@Override
	public Promise<String> getSessionToken(String username) {

		return getResource(username)
				.then(res -> {

					if (res == null) {

						throw new NotRegisteredException("Utente " + username + " non registrato");
					}

					return Promise.resolve(res.getSessionToken());
				});
	}

	@Override
	public Promise<Void> create(String username, String hash, String sessionToken) {

		return getResource(username)
				.then(res -> {

					if (res != null) {

						throw new AlreadyRegisteredException("Utente " + username + " già registrato");
					}

					return new IDBResource.Builder()
							.setUsername(username)
							.setHash(hash)
							.setSessionToken(sessionToken)
							.create()
							.then(this::persist)
							.then(null);
				});
	}

	@Override
	public Promise<Void> updateSessionToken(String username, String sessionToken) {

		return getResource(username)
				.then(res -> new IDBResource.Builder()
						.setUsername(username)
						.setHash(res != null ? res.getHash() : null)
						.setSessionToken(sessionToken)
						.setConcurrencyId(res != null ? res.getConcurrencyId() : null)
						.setEncData(res != null ? res.getEncData() : null)
						.create()
						.then(this::persist)
						.then(null));
	}

	@Override
	public Promise<Void> delete(String username) {

		return super.delete(username);
	}
}