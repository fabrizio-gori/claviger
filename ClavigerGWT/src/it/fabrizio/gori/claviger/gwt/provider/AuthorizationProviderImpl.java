package it.fabrizio.gori.claviger.gwt.provider;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.Consumer;

import com.google.gwt.user.client.Cookies;

import it.fabrizio.gori.claviger.core.Cipher;
import it.fabrizio.gori.claviger.core.Promise;
import it.fabrizio.gori.claviger.core.model.Authorization;
import it.fabrizio.gori.claviger.core.provider.AuthorizationProvider;
import it.fabrizio.gori.claviger.core.provider.exceptions.AlreadyRegisteredException;
import it.fabrizio.gori.claviger.core.provider.exceptions.NotRegisteredException;
import it.fabrizio.gori.claviger.core.provider.exceptions.UnauthenticatedException;
import it.fabrizio.gori.claviger.core.provider.exceptions.WrongPasswordException;
import it.fabrizio.gori.claviger.gwt.utils.UUID;

class AuthorizationProviderImpl extends AbstractProvider<UserAuthProvider> implements AuthorizationProvider {

	private static final String COOKIE_USERNAME = "claviger-current-username";
	private static final String COOKIE_ACCESSTOKEN = "claviger-current-accesstoken";

	public AuthorizationProviderImpl(Cipher cipher, Consumer<Throwable> logger,
			UserAuthProvider mainResourceProvider,
			UserAuthProvider... fallbackResourceProviders) {

		super(cipher, logger, mainResourceProvider, fallbackResourceProviders);
	}

	@Override
	public Promise<Authorization> get() {

		try {

			String username = Cookies.getCookie(COOKIE_USERNAME);
			String accessToken = Cookies.getCookie(COOKIE_ACCESSTOKEN);

			Objects.requireNonNull(username, "Username vuoto");
			Objects.requireNonNull(accessToken, "accessToken vuoto");

			List<Promise<Entry<String, Authorization>>> promises = new ArrayList<>();

			getFallbackProviders().forEach(p -> promises.add(p.getSessionToken(username)
					.then(sessionToken -> Promise.resolve(new SimpleImmutableEntry<String, Authorization>(sessionToken,
							new Authorization(username, decrypt(sessionToken, accessToken)))))));

			promises.add(getMainProvider().getSessionToken(username)
					.thenCatch(NotRegisteredException.class, ex -> {

						getFallbackProviders().forEach(p -> p.delete(username));

						throw ex;

					}).then(sessionToken -> Promise.resolve(new SimpleImmutableEntry<String, Authorization>(sessionToken,
							new Authorization(username, decrypt(sessionToken, accessToken))))));

			return Promise.firstFulfilled(promises)
					.then(entry -> {

						Promise.all(createPromiseForAll(p -> p.updateSessionToken(username, entry.getKey()).thenCatch(ex -> {

							log(ex);

							return null;
						})));

						return Promise.resolve(entry.getValue());

					}).thenCatch(ex -> {

						Cookies.removeCookie(COOKIE_USERNAME);
						Cookies.removeCookie(COOKIE_ACCESSTOKEN);

						throw ex;
					});

		} catch (Throwable t) {

			Cookies.removeCookie(COOKIE_USERNAME);
			Cookies.removeCookie(COOKIE_ACCESSTOKEN);

			return Promise.reject(new UnauthenticatedException());
		}
	}

	@Override
	public Promise<Void> revoke(Authorization auth) {

		try {

			Cookies.removeCookie(COOKIE_USERNAME);
			Cookies.removeCookie(COOKIE_ACCESSTOKEN);

			Objects.requireNonNull(auth, "Autorizzazione vuota");

			Promise.all(createPromiseForAll(p -> p.updateSessionToken(auth.getUsername(), null).thenCatch(ex -> {

				log(ex);

				return null;
			})));

			return Promise.resolve(null);

		} catch (Throwable t) {

			return Promise.reject(new UnauthenticatedException());
		}
	}

	@Override
	public Promise<Authorization> authenticate(String username, String password) {

		try {

			if (username != null && username.isEmpty()) {

				throw new Exception("Specificare uno username");
			}

			String hash = hash(password);

			Objects.requireNonNull(hash, "Hash vuoto");

			List<Promise<String>> promises = new ArrayList<>();

			getFallbackProviders().forEach(p -> promises.add(p.getHash(username)
					.thenCatch(ex -> new Promise<>((resolve, reject) -> {

						log(ex);
					}))));

			promises.add(getMainProvider().getHash(username)
					.thenCatch(NotRegisteredException.class, ex -> {

						getFallbackProviders().forEach(p -> p.delete(username));

						throw ex;
					}));

			return Promise.race(promises)
					.then(h -> {

						if (h != hash) {

							throw new WrongPasswordException();
						}

						String accessToken = UUID.generateSecureRandom();
						String sessionToken = encrypt(password, accessToken);

						Promise.all(createPromiseForAll(p -> p.updateSessionToken(username, sessionToken).thenCatch(ex -> {

							log(ex);

							return null;
						})));

						Cookies.setCookie(COOKIE_USERNAME, username);
						Cookies.setCookie(COOKIE_ACCESSTOKEN, accessToken);

						return Promise.resolve(new Authorization(username, password));

					});

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}

	@Override
	public Promise<Authorization> signUp(String username, String password) {

		try {

			if (username != null && username.isEmpty()) {

				throw new Exception("Specificare uno username");
			}

			if (password != null && password.isEmpty()) {

				throw new Exception("Specificare una password");
			}

			String hash = hash(password);
			String accessToken = UUID.generateSecureRandom();
			String sessionToken = encrypt(password, accessToken);

			Objects.requireNonNull(hash, "Hash vuoto");
			Objects.requireNonNull(accessToken, "Access token vuoto");
			Objects.requireNonNull(sessionToken, "Session token vuoto");

			return getMainProvider().create(username, hash, sessionToken)
					.then(vd -> {

						Promise.all(createPromiseForFallbacks(p -> p.create(username, hash, sessionToken)
								.thenCatch(AlreadyRegisteredException.class, ex -> p.delete(username)
										.then(v -> p.create(username, hash, sessionToken)))
								.thenCatch(ex -> {

									log(ex);

									return null;
								})));

						Cookies.setCookie(COOKIE_USERNAME, username);
						Cookies.setCookie(COOKIE_ACCESSTOKEN, accessToken);

						return Promise.resolve(new Authorization(username, password));
					});

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}
}