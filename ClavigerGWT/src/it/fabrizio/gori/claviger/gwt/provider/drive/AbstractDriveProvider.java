package it.fabrizio.gori.claviger.gwt.provider.drive;

import com.google.gwt.user.client.Timer;

import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.Promise;
import it.fabrizio.gori.claviger.core.Promise.Rejecter;
import it.fabrizio.gori.claviger.core.Promise.Resolver;
import it.fabrizio.gori.claviger.core.provider.exceptions.NotRegisteredException;
import it.fabrizio.gori.claviger.core.view.View;
import it.fabrizio.gori.claviger.gwt.TimeoutException;
import it.fabrizio.gori.claviger.gwt.gapi.GAPI;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIAuth.PromptValues;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIAuth.SignInOptions;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIDrive;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIFile;
import it.fabrizio.gori.claviger.gwt.gapi.GAPIUtil;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

class AbstractDriveProvider {

	private final Application application;

	public AbstractDriveProvider(Application application) {

		this.application = application;
	}

	@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
	static class FolderProp {

		@JsProperty
		public native String getSessionToken();

		@JsProperty
		public native void setSessionToken(String accessToken);

		@JsProperty
		public native String getHash();

		@JsProperty
		public native void setHash(String password);
	}

	@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
	static class FileProp {

		@JsProperty
		public native String getConcurrencyId();

		@JsProperty
		public native void setConcurrencyId(String concurrencyId);
	}

	static {

		GAPI.init("232898934286-kgg6m357d1dunbl3cechech14alntesi.apps.googleusercontent.com",
				"https://www.googleapis.com/auth/drive.file",
				"https://www.googleapis.com/discovery/v1/apis/drive/v3/rest");
	}

	protected static final String FOLDER_MIMETYPE = "application/vnd.google-apps.folder";

	private static final String CLAVIGER_FOLDER = "Claviger";

	private static GAPIFile<?> clavigerFolder = null;
	private static boolean showingLogin = false;

	protected <T> Promise<T> createTimeoutPromise(Promise<T> promise) {

		return new Promise<T>((resolve, reject) -> {

			final int TIMEOUT_MILLIS = 10000;

			final Timer timeoutFail = new Timer() {

				@Override
				public void run() {

					reject.accept(new TimeoutException());
				}
			};

			timeoutFail.schedule(TIMEOUT_MILLIS);

			promise.then(x -> {

				timeoutFail.cancel();

				resolve.accept(x);

				return null;

			}).thenCatch(ex -> {

				timeoutFail.cancel();

				reject.accept(ex);

				return null;
			});
		});
	}

	protected Promise<GAPI> getGAPIInstance() {

		return createTimeoutPromise(GAPI.get().then(GAPI::load).then(GAPI::init)).then(this::signIn);
	}

	protected Promise<GAPIFile<?>> searchClavigerFolder(GAPI gapi) {

		return createTimeoutPromise(new Promise<GAPIFile<?>>((resolve, reject) -> {

			if (clavigerFolder == null) {

				GAPIDrive.ListOptions opts = new GAPIDrive.ListOptions();

				opts.fields(GAPIFile.Fields.ID);
				opts.searchQuery("name = '" + CLAVIGER_FOLDER + "' and mimeType = '" + FOLDER_MIMETYPE + "' and trashed = false");

				gapi.getClient().getDrive().list(opts).then(r -> {

					if (r.getStatus() == 200) {

						if (r.getResult().files().length > 1) {

							reject.accept(new Exception("Multiple Claviger folder found."));

						} else if (r.getResult().files().length <= 0) {

							reject.accept(new GAPIFile.NotFoundException("Claviger folder not found"));

						} else {

							clavigerFolder = r.getResult().files()[0];
							resolve.accept(clavigerFolder);
						}

					} else {

						reject.accept(GAPIUtil.asError(r.getResult()));
					}

					return null;

				}).thenCatch(err -> {

					reject.accept(GAPIUtil.asError(err));

					return null;
				});

			} else {

				resolve.accept(clavigerFolder);
			}

		})).thenCatch(GAPIFile.NotFoundException.class, ex -> createClavigerFolder(gapi));
	}

	public Promise<Void> delete(String username) {

		return getGAPIInstance()
				.then(this::searchClavigerFolder)
				.then(clavigerFolder -> searchUserFolder(clavigerFolder, username))
				.then(userFolder -> {

					userFolder.gapi().getClient().getDrive().delete(userFolder.getId());

					return null;
				});
	}

	protected Promise<GAPIFile<FolderProp>> searchUserFolder(GAPIFile<?> parent, String folderName) {

		return createTimeoutPromise(new Promise<GAPIFile<FolderProp>>((resolve, reject) -> {

			GAPIDrive.ListOptions opts = new GAPIDrive.ListOptions();

			opts.fields(GAPIFile.Fields.ID, GAPIFile.Fields.NAME, GAPIFile.Fields.APP_PROPERTIES);
			opts.searchQuery("name = '" + folderName + "' and mimeType = '" + FOLDER_MIMETYPE + "' and '" + parent.getId()
					+ "' in parents and trashed = false");

			parent.gapi().getClient().getDrive().list(opts).then(r -> {

				if (r.getStatus() == 200) {

					if (r.getResult().files().length > 1) {

						reject.accept(new Exception("Multiple file found."));

					} else if (r.getResult().files().length <= 0) {

						reject.accept(new NotRegisteredException("Utente " + folderName + " non registrato"));

					} else {

						resolve.accept(r.getResult().<FolderProp>files()[0]);
					}

				} else {

					reject.accept(GAPIUtil.asError(r.getResult()));
				}

				return null;

			}).thenCatch(err -> {

				reject.accept(GAPIUtil.asError(err));

				return null;
			});
		}));
	}

	private Promise<GAPI> signIn(GAPI gapi) {

		return new Promise<GAPI>((resolve, reject) -> {

			if (gapi.getAuthInstance().isSignedIn()) {

				resolve.accept(gapi);

			} else {

				showLogin(gapi, resolve, reject);
			}
		});
	}

	private void showLogin(GAPI gapi, Resolver<GAPI> resolve, Rejecter reject) {

		if (!showingLogin) {

			showingLogin = true;

			View<?> currentView = application.getCurrentView();

			currentView.setEnabled(false);

			application.getViewFactory().createQuestionView().open("Claviger",
					"Devi accedere al tuo account google.\n\nContinuare?")
					.whenYes(() -> {

						SignInOptions signInOptions = new SignInOptions();
						signInOptions.prompt(PromptValues.SELECT_ACCOUNT);

						gapi.getAuthInstance().signIn(signInOptions)

								.then(gu -> {

									resolve.accept(gapi);

									return null;

								}).thenCatch(err -> {

									String errMessage = GAPIUtil.asString(err);

									if ("popup_closed_by_user" == errMessage) {

										reject.accept(new Exception("Il login al tuo account google è necessario."));

									} else {

										reject.accept(new Exception(errMessage));
									}

									return null;

								}).then(v -> {

									showingLogin = false;

									return null;

								}).thenAlways(() -> {

									currentView.setEnabled(true);
								});

					}).whenNo(() -> {

						reject.accept(new Exception("Il login al tuo account google è necessario."));

						showingLogin = false;

						currentView.setEnabled(true);
					});
		}
	}

	private Promise<GAPIFile<FolderProp>> createClavigerFolder(GAPI gapi) {

		return createTimeoutPromise(new Promise<GAPIFile<FolderProp>>((resolve, reject) -> {

			GAPIFile<FolderProp> userFolder = new GAPIFile<FolderProp>();

			userFolder.setName(CLAVIGER_FOLDER);
			userFolder.setMimeType(FOLDER_MIMETYPE);

			gapi.getClient().getDrive().create(userFolder).then(resp -> {

				if (resp.getStatus() == 200) {

					resolve.accept(userFolder);

				} else {

					reject.accept(GAPIUtil.asError(resp.getResult()));
				}

				return null;
			}).thenCatch(err -> {

				reject.accept(GAPIUtil.asError(err));

				return null;
			});
		}));
	}
}