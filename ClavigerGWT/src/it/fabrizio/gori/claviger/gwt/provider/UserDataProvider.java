package it.fabrizio.gori.claviger.gwt.provider;

import it.fabrizio.gori.claviger.core.Promise;

public interface UserDataProvider {

	public static class Data {

		public static class Builder {

			private Data result = new Data();

			public Builder setEncData(String encData) {

				result.encData = encData;

				return this;
			}

			public Builder setConcurrencyId(String concurrencyId) {

				result.concurrencyId = concurrencyId;

				return this;
			}

			public Data create() {

				return result;
			}
		}

		private String encData;
		private String concurrencyId;

		private Data() {

		}

		public Data(String encData, String concurrencyId) {

			this.encData = encData;
			this.concurrencyId = concurrencyId;
		}

		public String getEncData() {

			return encData;
		}

		public String getConcurrencyId() {

			return concurrencyId;
		}
	}

	Promise<Data> getData(String username, String hash);

	Promise<String> update(String username, String hash, String concurrencyId, String encData);

	Promise<Void> refresh(String username, String hash, String concurrencyId, String encData);

	Promise<Void> delete(String username);
}