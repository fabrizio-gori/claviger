package it.fabrizio.gori.claviger.gwt.provider;

import it.fabrizio.gori.claviger.core.Promise;

public interface UserAuthProvider {

	Promise<String> getHash(String username);

	Promise<String> getSessionToken(String username);

	Promise<Void> create(String username, String hash, String sessionToken);

	Promise<Void> updateSessionToken(String username, String sessionToken);

	Promise<Void> delete(String username);
}