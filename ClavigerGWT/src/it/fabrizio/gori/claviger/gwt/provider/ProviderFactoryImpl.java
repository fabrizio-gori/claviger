package it.fabrizio.gori.claviger.gwt.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import elemental2.dom.DomGlobal;
import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.ProviderFactory;
import it.fabrizio.gori.claviger.core.model.Account;
import it.fabrizio.gori.claviger.core.provider.AuthorizationProvider;
import it.fabrizio.gori.claviger.core.provider.DataProvider;
import it.fabrizio.gori.claviger.gwt.provider.drive.UserAuthProviderDriveImpl;
import it.fabrizio.gori.claviger.gwt.provider.drive.UserDataProviderDriveImpl;
import it.fabrizio.gori.claviger.gwt.provider.idb.UserAuthProviderIDBImpl;
import it.fabrizio.gori.claviger.gwt.provider.idb.UserDataProviderIDBImpl;

public class ProviderFactoryImpl implements ProviderFactory {

	private static final Consumer<Throwable> GWT_LOGGER = t -> DomGlobal.console.log(t.getMessage());

	private static final Function<List<Account>, String> ACCOUNT_TO_STRING = data -> {

		String stringData = "";

		for (Account i : data) {

			stringData += i.getDescription() + "\n";

			for (String j : i.getInfos()) {

				stringData += "\t" + j + "\n";
			}

			stringData += "\n";
		}

		return stringData;
	};

	private static final Function<String, List<Account>> ACCOUNT_FROM_STRING = stringData -> {

		List<Account> result = new ArrayList<Account>();

		if (stringData != null) {

			Account account = null;

			for (String i : stringData.replaceAll("\r", "").trim().split("\n")) {

				if (i != null && !"".equals(i.trim())) {

					if (i.matches("^\\S(.)*")) {

						account = new Account(i.toUpperCase().trim());
						result.add(account);

					} else if (account != null) {

						account.getInfos().add(i.substring(1));
					}
				}
			}
		}

		return result;
	};

	private final Application application;

	public ProviderFactoryImpl(Application application) {

		this.application = application;
	}

	@Override
	public AuthorizationProvider getAuthorizationProvider() {

		return new AuthorizationProviderImpl(application.getCipher(), GWT_LOGGER,
				new UserAuthProviderDriveImpl(application),
				new UserAuthProviderIDBImpl("Claviger"));
	}

	@Override
	public DataProvider<Account> getAccountProvider() {

		return new DataProviderImpl<Account>(application, GWT_LOGGER, ACCOUNT_FROM_STRING, ACCOUNT_TO_STRING,
				new UserDataProviderDriveImpl(application, "data.bin"),
				new UserDataProviderIDBImpl("Claviger"));
	}

	@Override
	public String getVersion() {

		return "v1.0";
	}

}
