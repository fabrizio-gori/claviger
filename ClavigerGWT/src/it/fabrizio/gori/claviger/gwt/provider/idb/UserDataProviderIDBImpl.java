package it.fabrizio.gori.claviger.gwt.provider.idb;

import java.util.Objects;

import it.fabrizio.gori.claviger.core.Promise;
import it.fabrizio.gori.claviger.core.provider.exceptions.ConcurrencyException;
import it.fabrizio.gori.claviger.core.provider.exceptions.UnauthorizedException;
import it.fabrizio.gori.claviger.gwt.provider.UserDataProvider;
import it.fabrizio.gori.claviger.gwt.utils.UUID;

public class UserDataProviderIDBImpl extends AbstractIDBProvider implements UserDataProvider {

	public UserDataProviderIDBImpl(String idbName) {

		super(idbName);
	}

	@Override
	public Promise<Data> getData(String username, String hash) {

		return getResource(username)
				.then(res -> checkHash(res, hash))
				.then(res -> Promise.resolve(new Data(res.getEncData(), res.getConcurrencyId())));
	}

	@Override
	public Promise<String> update(String username, String hash, String concurrencyId, String encData) {

		return getResource(username)
				.then(res -> checkHash(res, hash))
				.then(res -> checkConcurrency(res, concurrencyId))
				.then(builder -> builder
						.setEncData(encData)
						.create()
						.then(this::persist)
						.then(res -> Promise.resolve(res.getConcurrencyId())));
	}

	@Override
	public Promise<Void> refresh(String username, String hash, String concurrencyId, String encData) {

		return getResource(username)
				.then(res -> new IDBResource.Builder()
						.setUsername(username)
						.setHash(hash)
						.setSessionToken(res != null ? res.getSessionToken() : null)
						.setConcurrencyId(concurrencyId)
						.setEncData(encData)
						.create()
						.then(this::persist)
						.then(null));
	}

	@Override
	public Promise<Void> delete(String username) {

		return super.delete(username);
	}

	private Promise<IDBResource.Builder> checkConcurrency(IDBResource resource, String concurrencyId) {

		try {

			Objects.requireNonNull(resource, "Risorsa vuota");

			if (resource.getConcurrencyId() != concurrencyId) {

				throw new ConcurrencyException();
			}

			return Promise.resolve(new IDBResource.Builder()
					.setUsername(resource.getUsername())
					.setHash(resource.getHash())
					.setSessionToken(resource.getSessionToken())
					.setConcurrencyId(UUID.generateRandom()));

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}

	private Promise<IDBResource> checkHash(IDBResource resource, String hash) {

		try {

			Objects.requireNonNull(resource, "Risorsa vuota");
			Objects.requireNonNull(resource.getHash(), "L'hash della risorsa è vuoto");
			Objects.requireNonNull(hash, "Hash vuoto");

			if (resource.getHash() != hash) {

				throw new UnauthorizedException();
			}

			return Promise.resolve(resource);

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}
}
