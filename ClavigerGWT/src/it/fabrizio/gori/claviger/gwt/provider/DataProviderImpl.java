package it.fabrizio.gori.claviger.gwt.provider;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.Promise;
import it.fabrizio.gori.claviger.core.Promise.FirstFulfilledException;
import it.fabrizio.gori.claviger.core.model.Authorization;
import it.fabrizio.gori.claviger.core.provider.DataProvider;
import it.fabrizio.gori.claviger.core.provider.exceptions.NotRegisteredException;
import it.fabrizio.gori.claviger.core.provider.exceptions.UnauthorizedException;

class DataProviderImpl<T> extends AbstractProvider<UserDataProvider> implements DataProvider<T> {

	private final Function<String, List<T>> fromString;
	private final Function<List<T>, String> toString;
	private final Application application;

	public DataProviderImpl(Application application,
			Consumer<Throwable> logger,
			Function<String, List<T>> fromString,
			Function<List<T>, String> toString,
			UserDataProvider mainResourceProvider,
			UserDataProvider... fallbackResourceProviders) {

		super(application.getCipher(), logger, mainResourceProvider, fallbackResourceProviders);

		this.fromString = fromString;
		this.toString = toString;
		this.application = application;
	}

	private String retrievedFallbackConcurrencyId;

	@Override
	public Promise<DataResult<T>> getAll(Authorization auth, Comparator<T> sortComparator) {

		try {

			if (auth == null) {

				throw new UnauthorizedException();
			}

			retrievedFallbackConcurrencyId = null;

			String hash = hash(auth.getPassword());

			List<Promise<DataResult<T>>> promises = new ArrayList<>();

			getFallbackProviders().forEach(p -> promises.add(p.getData(auth.getUsername(), hash)
					.then(data -> {

						if (data.getConcurrencyId() == null) {

							throw new Exception();
						}

						retrievedFallbackConcurrencyId = data.getConcurrencyId();

						List<T> dataList = fromString.apply(decrypt(data.getEncData(), auth.getPassword()));

						dataList.sort(sortComparator);

						return Promise.resolve(DataResult.<T>builder()
								.setConcurrencyId(data.getConcurrencyId())
								.setData(dataList)
								.create());
					})));

			promises.add(getMainProvider().getData(auth.getUsername(), hash)
					.then(data -> {

						if (retrievedFallbackConcurrencyId != data.getConcurrencyId()) {

							if (retrievedFallbackConcurrencyId != null) {

								application.getCurrentView().busy();

								application.getViewFactory().createInfoView().open("Claviger",
										"I dati non sono aggiornati, prima di salvare sarà necessario aggiornarli.")
										.whenClose(() -> application.getCurrentView().ready());
							}

							List<T> dataList = fromString.apply(decrypt(data.getEncData(), auth.getPassword()));

							Promise.all(createPromiseForFallbacks(p -> p.refresh(auth.getUsername(), hash, data.getConcurrencyId(), data.getEncData())
									.thenCatch(ex -> {

										log(ex);

										return null;
									})));

							return Promise.resolve(DataResult.<T>builder()
									.setConcurrencyId(data.getConcurrencyId())
									.setData(dataList)
									.create());

						} else {

							return null;
						}

					}).thenCatch(NotRegisteredException.class, ex -> {

						getFallbackProviders().forEach(p -> p.delete(auth.getUsername()));

						throw new UnauthorizedException();
					}));

			return Promise.firstFulfilled(promises)
					.thenCatch(FirstFulfilledException.class, ex -> {

						throw ex.ifContains(UnauthorizedException.class)
								.orElse(ex);
					});

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}

	@Override
	public Promise<String> save(Authorization auth, String concurrencyId, List<T> data) {

		try {

			if (auth == null) {

				throw new UnauthorizedException();
			}

			String hash = hash(auth.getPassword());
			String encData = encrypt(toString.apply(data), auth.getPassword());

			return getMainProvider().update(auth.getUsername(), hash, concurrencyId, encData)
					.then(newConcurrencyId -> {

						return Promise.all(createPromiseForFallbacks(p -> p.refresh(auth.getUsername(), hash, newConcurrencyId, encData)
								.thenCatch(ex -> {

									log(ex);

									return null;

								}))).then(resultList -> Promise.resolve(newConcurrencyId));
					});

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}
}