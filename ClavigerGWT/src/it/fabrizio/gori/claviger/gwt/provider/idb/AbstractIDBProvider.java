package it.fabrizio.gori.claviger.gwt.provider.idb;

import java.util.Objects;

import elemental2.dom.Worker;
import it.fabrizio.gori.claviger.core.Promise;
import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

public class AbstractIDBProvider {

	@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
	protected static class IDBResource {

		static public class Builder {

			private final IDBResource result = new IDBResource();

			public Builder setUsername(String username) {

				result.setUsername(username);

				return this;
			}

			public Builder setHash(String hash) {

				result.setHash(hash);

				return this;
			}

			public Builder setConcurrencyId(String concurrencyId) {

				result.setConcurrencyId(concurrencyId);

				return this;
			}

			public Builder setEncData(String encData) {

				result.setEncData(encData);

				return this;
			}

			public Builder setSessionToken(String sessionToken) {

				result.setSessionToken(sessionToken);

				return this;
			}

			public Promise<IDBResource> create() {

				try {

					Objects.requireNonNull(result.getUsername());

					return Promise.resolve(result);

				} catch (Throwable t) {

					return Promise.reject(t);
				}
			}
		}

		private IDBResource() {

		}

		@JsProperty
		native public String getUsername();

		@JsProperty
		native private void setUsername(String username);

		@JsProperty
		native public String getHash();

		@JsProperty
		native private void setHash(String hash);

		@JsProperty
		native public String getSessionToken();

		@JsProperty
		native private void setSessionToken(String sessionToken);

		@JsProperty
		native public String getConcurrencyId();

		@JsProperty
		native private void setConcurrencyId(String concurrencyId);

		@JsProperty
		native public String getEncData();

		@JsProperty
		native private void setEncData(String encData);
	}

	@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
	private static class IDBRequest {

		private static enum Operations {

			GET,
			PUT,
			DELETE;

			@Override
			public String toString() {

				return super.name();
			}

			private native static void setOperation(IDBRequest opt, Operations op) /*-{

				opt["operation"] = op.toString();
			}-*/;
		}

		@JsProperty(name = "idbDefinition")
		public native void setIDBDefinition(IDBDefinition idbDefinition);

		@JsOverlay
		public final void setOperation(Operations operation) {

			Operations.setOperation(this, operation);
		}

		@JsProperty
		public native void setParam(Object param);
	}

	@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
	private static class IDBDefinition {

		@JsProperty
		public native void setDbName(String dbName);

		@JsProperty
		public native void setDbVersion(int dbVersion);

		@JsProperty
		public native void setObjectStoreName(String objectStoreName);

		@JsProperty
		public native void setKeyPath(String keyPath);
	}

	private static final String KEY_PATH = "username";
	private static final String OBJECT_STORE_NAME = "clavigerResources";
	private static final int IDB_VER = 1;

	private final Worker idbWorker = new Worker("workers/idb-worker.js");
	private final IDBDefinition idbDefinition = new IDBDefinition();
	private final IDBRequest idbRequest = new IDBRequest();

	protected AbstractIDBProvider(String idbName) {

		idbDefinition.setDbName(idbName);
		idbDefinition.setDbVersion(IDB_VER);
		idbDefinition.setKeyPath(KEY_PATH);
		idbDefinition.setObjectStoreName(OBJECT_STORE_NAME);

		idbRequest.setIDBDefinition(idbDefinition);
	}

	protected Promise<IDBResource> getResource(String username) {

		try {

			Objects.requireNonNull(username, "Username vuoto");

			return new Promise<IDBResource>((resolve, reject) -> {

				idbWorker.onerror = evt -> {

					reject.accept(new Exception("Unable to get resource"));

					evt.preventDefault();

					return null;
				};

				idbWorker.onmessage = evt -> {

					resolve.accept((IDBResource) evt.data);

					return null;
				};

				idbRequest.setOperation(IDBRequest.Operations.GET);
				idbRequest.setParam(username);

				idbWorker.postMessage(idbRequest);
			});

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}

	protected Promise<Void> delete(String username) {

		try {

			Objects.requireNonNull(username, "Username vuoto");

			return new Promise<Void>((resolve, reject) -> {

				idbWorker.onerror = evt -> {

					reject.accept(new Exception("Unable to delete resource"));

					evt.preventDefault();

					return null;
				};

				idbWorker.onmessage = evt -> {

					resolve.accept(null);

					return null;
				};

				idbRequest.setOperation(IDBRequest.Operations.DELETE);
				idbRequest.setParam(username);

				idbWorker.postMessage(idbRequest);
			});

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}

	protected Promise<IDBResource> persist(IDBResource resource) {

		try {

			Objects.requireNonNull(resource, "Risorsa vuota");

			return new Promise<IDBResource>((resolve, reject) -> {

				idbWorker.onerror = evt -> {

					reject.accept(new Exception("Unable to presist resource"));

					evt.preventDefault();

					return null;
				};

				idbWorker.onmessage = evt -> {

					resolve.accept((IDBResource) evt.data);

					return null;
				};

				idbRequest.setOperation(IDBRequest.Operations.PUT);
				idbRequest.setParam(resource);

				idbWorker.postMessage(idbRequest);
			});

		} catch (Throwable t) {

			return Promise.reject(t);
		}
	}
}