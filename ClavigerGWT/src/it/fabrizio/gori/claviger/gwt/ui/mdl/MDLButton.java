package it.fabrizio.gori.claviger.gwt.ui.mdl;

import com.google.gwt.user.client.ui.Button;

public class MDLButton extends Button {

	public MDLButton() {

		this(null);
	}

	public MDLButton(String text) {

		setStyleName("mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored");

		getElement().getStyle().setProperty("color", "white");
		getElement().getStyle().setProperty("lineHeight", "1.5rem");
		getElement().getStyle().setProperty("height", "1.5rem");
		getElement().getStyle().setProperty("verticalAlign", "top");

		setText(text);
	}
}