package it.fabrizio.gori.claviger.gwt.ui.mdl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.impl.FocusImpl;

public final class MDLInput extends Widget implements TakesValue<String>, HasEnabled, Focusable {

	@UiTemplate("xml/MDLInput.ui.xml")
	protected interface UiBinderImpl extends UiBinder<Element, MDLInput> {
	}

	private static final FocusImpl impl = FocusImpl.getFocusImplForWidget();
	private static UiBinderImpl uiBinder = GWT.create(UiBinderImpl.class);

	@UiField
	protected Element inputWrapperElement;
	@UiField
	protected InputElement inputElement;
	@UiField
	protected Element labelElement;

	public MDLInput() {

		setElement(uiBinder.createAndBindUi(this));

		inputElement.setPropertyString("type", "text");
	}

	@Override
	public String getValue() {

		return inputElement.getValue();
	}

	@Override
	public void setValue(String value) {

		inputElement.setValue(value);
	}

	public void setType(String type) {

		inputElement.setPropertyString("type", type);
	}

	public void setLabel(String label) {

		labelElement.setInnerText(label);
	}

	public void setAutocomplete(String value) {

		inputElement.setPropertyString("autocomplete", value);
	}

	public void setAutocorrect(String value) {

		inputElement.setPropertyString("autocorrect", value);
	}

	public void setAutocapitalize(String value) {

		inputElement.setPropertyString("autocapitalize", value);
	}

	public void setSpellcheck(String value) {

		inputElement.setPropertyString("spellcheck", value);
	}

	@Override
	public int getTabIndex() {

		return impl.getTabIndex(inputElement);
	}

	@Override
	public void setAccessKey(char key) {

		impl.setAccessKey(inputElement, key);
	}

	@Override
	public void setFocus(boolean focused) {

		if (focused) {

			impl.focus(inputElement);

		} else {

			impl.blur(inputElement);
		}
	}

	@Override
	public void setTabIndex(int index) {

		impl.setTabIndex(inputElement, index);
	}

	@Override
	public boolean isEnabled() {

		return !inputElement.getPropertyBoolean("disabled");
	}

	@Override
	public void setEnabled(boolean enabled) {

		if (enabled) {

			inputWrapperElement.removeClassName("is-disabled");

		} else {

			inputWrapperElement.addClassName("is-disabled");
		}

		inputElement.setPropertyBoolean("disabled", !enabled);
	}
}