package it.fabrizio.gori.claviger.gwt.ui.mdl;

import java.util.function.Function;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Widget;

public class MDLEditable extends Widget {

	@UiTemplate("xml/MDLEditable.ui.xml")
	protected interface UiBinderImpl extends UiBinder<Element, MDLEditable> {
	}

	private static UiBinderImpl uiBinder = GWT.create(UiBinderImpl.class);

	private Function<String, String> converter = null;

	@UiField
	protected Element contentElement;

	public MDLEditable() {

		setElement(uiBinder.createAndBindUi(this));

		setup(contentElement, s -> {

			if (converter != null) {

				return converter.apply(s);

			} else {

				return s;
			}
		});
	}

	private native void setup(Element el, Function<String, String> c) /*-{

		el.getHTMLFromBase64Data = function(base64Data) {

			return c.@java.util.function.Function::apply(*)(base64Data);
		};
	}-*/;

	public void setConverter(Function<String, String> converter) {

		this.converter = converter;
	}

	public void setEditable(boolean editable) {

		contentElement.setAttribute("contenteditable", editable ? "true" : "false");
	}

	public boolean isEditable() {

		return contentElement.getAttribute("contenteditable") == "true";
	}

	public void add(Widget w) {

		DOM.appendChild(contentElement, w.getElement());
	}

	public void clear() {

		contentElement.removeAllChildren();
	}
}
