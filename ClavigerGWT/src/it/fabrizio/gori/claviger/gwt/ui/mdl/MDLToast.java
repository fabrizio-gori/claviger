package it.fabrizio.gori.claviger.gwt.ui.mdl;

import java.util.function.Consumer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

public class MDLToast extends Widget {

	@UiTemplate("xml/MDLToast.ui.xml")
	protected interface UiBinderImpl extends UiBinder<Element, MDLToast> {
	}

	private static UiBinderImpl uiBinder = GWT.create(UiBinderImpl.class);
	private static final MDLToast toast = new MDLToast();

	private MDLToast() {

		setElement(uiBinder.createAndBindUi(this));

		MDLComponentHandler.upgradeElement(getElement());
	}

	public static void show(String message, Consumer<Void> callback) {

		if (!toast.isAttached()) {

			RootPanel.get().add(toast);

			Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {

				@Override
				public void execute() {

					showImpl(toast.getElement(), message, callback);
				}
			});

		} else {

			showImpl(toast.getElement(), message, callback);
		}
	}

	public static void show(String message) {

		if (!toast.isAttached()) {

			RootPanel.get().add(toast);

			Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {

				@Override
				public void execute() {

					showImpl(toast.getElement(), message);
				}
			});

		} else {

			showImpl(toast.getElement(), message);
		}
	}

	private static native void showImpl(Element el, String message, Consumer<Void> callback) /*-{

		el.MaterialSnackbar.showSnackbar({

			message : message,
			actionHandler : function(e) {
				callback.@java.util.function.Consumer::accept(*)(null);
			},
			actionText : 'Undo'
		});
	}-*/;

	private static native void showImpl(Element el, String message) /*-{

		el.MaterialSnackbar.showSnackbar({

			message : message
		});
	}-*/;
}