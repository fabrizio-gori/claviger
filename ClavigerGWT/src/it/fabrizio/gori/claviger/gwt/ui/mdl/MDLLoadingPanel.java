package it.fabrizio.gori.claviger.gwt.ui.mdl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Widget;

public class MDLLoadingPanel extends Widget {

	@UiTemplate("xml/MDLLoadingPanel.ui.xml")
	protected interface UiBinderImpl extends UiBinder<Element, MDLLoadingPanel> {
	}

	private static UiBinderImpl uiBinder = GWT.create(UiBinderImpl.class);

	@UiField
	protected Element waitElement;

	public MDLLoadingPanel() {

		setElement(uiBinder.createAndBindUi(this));

		MDLComponentHandler.upgradeElement(waitElement);
	}

	public void showWaiting(boolean show) {

		if (show) {

			waitElement.getStyle().setDisplay(Display.BLOCK);

		} else {

			waitElement.getStyle().setDisplay(Display.NONE);
		}
	}
}