package it.fabrizio.gori.claviger.gwt.ui.mdl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

import it.fabrizio.gori.claviger.gwt.view.mdl.resources.MDLResources;

public class MDLWindow extends Composite {

	public static enum HeaderSize {

		SMALL, BIG;
	}

	@UiTemplate("xml/MDLWindow.ui.xml")
	protected interface UiBinderImpl extends UiBinder<Widget, MDLWindow> {
	}

	private static UiBinderImpl uiBinder = GWT.create(UiBinderImpl.class);

	@UiField
	protected Panel headerWrapper;
	@UiField
	protected Panel bodyWrapper;
	@UiField
	protected Panel footerWrapper;

	private boolean hasFooter = false;
	private HeaderSize headerSize = HeaderSize.SMALL;

	public MDLWindow() {

		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiChild(tagname = "header", limit = 1)
	public void setHeader(Widget header, HeaderSize size) {

		this.headerSize = size;

		if (HeaderSize.BIG.equals(size)) {

			headerWrapper.setStyleName(MDLResources.INSTANCE.style().mdlWindowContentHeader() + " "
					+ MDLResources.INSTANCE.style().mdlWindowContentHeaderBig());

		} else {

			headerWrapper.setStyleName(MDLResources.INSTANCE.style().mdlWindowContentHeader() + " "
					+ MDLResources.INSTANCE.style().mdlWindowContentHeaderSmall());
		}

		refreshBodyClassName();

		headerWrapper.add(header);
	}

	@UiChild(tagname = "body", limit = 1)
	public void setBody(Widget body) {

		bodyWrapper.add(body);
	}

	@UiChild(tagname = "footer", limit = 1)
	public void setFooter(Widget footer) {

		if (footer != null) {

			footerWrapper.add(footer);

			footerWrapper.setVisible(true);

			hasFooter = true;

		} else {

			footerWrapper.setVisible(false);

			footerWrapper.clear();

			hasFooter = false;
		}

		refreshBodyClassName();
	}

	private void refreshBodyClassName() {

		if (hasFooter) {

			if (HeaderSize.BIG.equals(headerSize)) {

				bodyWrapper.setStyleName(MDLResources.INSTANCE.style().mdlWindowContentBody() + " "
						+ MDLResources.INSTANCE.style().mdlWindowContentBodySmallFooter());

			} else {

				bodyWrapper.setStyleName(MDLResources.INSTANCE.style().mdlWindowContentBody() + " "
						+ MDLResources.INSTANCE.style().mdlWindowContentBodyBigFooter());
			}

		} else {

			if (HeaderSize.BIG.equals(headerSize)) {

				bodyWrapper.setStyleName(MDLResources.INSTANCE.style().mdlWindowContentBody() + " "
						+ MDLResources.INSTANCE.style().mdlWindowContentBodySmallNoFooter());

			} else {

				bodyWrapper.setStyleName(MDLResources.INSTANCE.style().mdlWindowContentBody() + " "
						+ MDLResources.INSTANCE.style().mdlWindowContentBodyBigNoFooter());
			}
		}
	}
}
