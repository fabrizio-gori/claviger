package it.fabrizio.gori.claviger.gwt.ui.mdl;

import com.google.gwt.user.client.ui.Label;

public class MDLErrorLabel extends Label {

	public MDLErrorLabel() {

		getElement().getStyle().setProperty("fontSize", "0.8rem");
		getElement().getStyle().setColor("red");
		getElement().getStyle().setProperty("marginBottom", "1rem");
	}
}