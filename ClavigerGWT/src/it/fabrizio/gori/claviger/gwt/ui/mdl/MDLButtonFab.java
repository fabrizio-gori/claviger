package it.fabrizio.gori.claviger.gwt.ui.mdl;

import com.google.gwt.user.client.ui.Button;

public class MDLButtonFab extends Button {

	public MDLButtonFab() {

		this(null);
	}

	public MDLButtonFab(String text) {

		setStyleName(
				"mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-button--accent");

		getElement().getStyle().setProperty("color", "white");

		setText(text);
	}
}