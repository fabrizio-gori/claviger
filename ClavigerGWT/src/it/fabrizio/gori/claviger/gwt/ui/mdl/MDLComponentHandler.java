package it.fabrizio.gori.claviger.gwt.ui.mdl;

import com.google.gwt.dom.client.Element;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "componentHandler")
public class MDLComponentHandler {

	public static native void upgradeDom();

	public static native void upgradeElement(Element element);
}