package it.fabrizio.gori.claviger.gwt.ui.mdl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.TouchEndEvent;
import com.google.gwt.event.dom.client.TouchMoveEvent;
import com.google.gwt.event.dom.client.TouchStartEvent;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public class MDLAppLayout extends Composite {

	public static interface Style extends CssResource {

		@ClassName("mdl-application__glass-blur")
		public String glassBlur();
	}

	@UiTemplate("xml/MDLAppLayout.ui.xml")
	protected interface UiBinderImpl extends UiBinder<Widget, MDLAppLayout> {
	}

	private static UiBinderImpl uiBinder = GWT.create(UiBinderImpl.class);

	@UiField
	static protected Style style;

	@UiField
	protected Panel menuWrapper;
	@UiField
	protected Panel glassWrapper;
	@UiField
	protected Panel headerWrapper;
	@UiField
	protected Panel contentWrapper;

	private class SwipeCoord {

		private int startX = 0, startY = 0, endX = 0, endY = 0;

		public int getStartX() {

			return startX;
		}

		public void setStartX(int startX) {

			this.startX = startX;
		}

		public int getStartY() {

			return startY;
		}

		public void setStartY(int startY) {

			this.startY = startY;
		}

		public int getEndX() {

			return endX;
		}

		public void setEndX(int endX) {

			this.endX = endX;
		}

		public int getEndY() {

			return endY;
		}

		public void setEndY(int endY) {

			this.endY = endY;
		}
	}

	private final SwipeCoord swipeCoord = new SwipeCoord();
	private boolean swiping = false;

	public MDLAppLayout() {

		initWidget(uiBinder.createAndBindUi(this));

		glassWrapper.addDomHandler(e -> hideMenu(), ClickEvent.getType());

		addDomHandler(e -> {

			int min = (int) Math.max(Math.min(Window.getClientWidth(), Window.getClientHeight()) * 0.1, 50),
					x = e.getTargetTouches().get(0).getScreenX(), y = e.getTargetTouches().get(0).getScreenY();

			if (x <= min || x >= Window.getClientWidth() - min || y <= min || y >= Window.getClientHeight() - min) {

				swiping = true;

				swipeCoord.setStartX(x);
				swipeCoord.setStartY(y);

			} else {

				swiping = false;

				swipeCoord.setStartX(0);
				swipeCoord.setStartY(0);
			}

		}, TouchStartEvent.getType());

		addDomHandler(e -> {

			if (swiping) {

				int min = (int) Math.max(Math.min(Window.getClientWidth(), Window.getClientHeight()) * 0.3, 50);

				int deltaX = swipeCoord.getEndX() - swipeCoord.getStartX();
				int deltaY = swipeCoord.getEndY() - swipeCoord.getStartY();

				if (deltaX * deltaX + deltaY * deltaY < min * min) {

				} else {

					if (deltaY == 0 || Math.abs(deltaX / deltaY) > 1) {

						if (deltaX > 0) {

							showMenu();

						} else {

							hideMenu();
						}

					} else {

						if (deltaY > 0) {

						} else {

						}
					}
				}
			}

			swipeCoord.setStartX(0);
			swipeCoord.setStartY(0);
			swipeCoord.setEndX(0);
			swipeCoord.setEndY(0);

			swiping = false;

		}, TouchEndEvent.getType());

		addDomHandler(e -> {

			if (swiping) {

				e.preventDefault();

				swipeCoord.setEndX(e.getTargetTouches().get(0).getScreenX());
				swipeCoord.setEndY(e.getTargetTouches().get(0).getScreenY());
			}

		}, TouchMoveEvent.getType());
	}

	@UiChild(tagname = "header", limit = 1)
	public void setHeader(Widget header) {

		headerWrapper.add(header);
	}

	@UiChild(tagname = "content", limit = 1)
	public void setContent(Widget content) {

		contentWrapper.add(content);
	}

	@UiChild(tagname = "menu", limit = 1)
	public void setMenu(Widget menu) {

		menuWrapper.add(menu);
	}

	public void showMenu() {

		glassWrapper.getElement().getStyle().setDisplay(Display.BLOCK);
		glassWrapper.addStyleName(style.glassBlur());
		menuWrapper.getElement().getStyle().setProperty("left", "0rem");
	}

	public void hideMenu() {

		glassWrapper.removeStyleName(style.glassBlur());
		menuWrapper.getElement().getStyle().setProperty("left", "-17rem");
		glassWrapper.getElement().getStyle().setDisplay(Display.NONE);
	}
}
