package it.fabrizio.gori.claviger.gwt;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

import it.fabrizio.gori.claviger.core.Application;
import it.fabrizio.gori.claviger.core.Cipher;
import it.fabrizio.gori.claviger.core.ProviderFactory;
import it.fabrizio.gori.claviger.core.ViewFactory;
import it.fabrizio.gori.claviger.gwt.cryptojs.CryptoJSCipherImpl;
import it.fabrizio.gori.claviger.gwt.provider.ProviderFactoryImpl;
import it.fabrizio.gori.claviger.gwt.view.mdl.MDLViewFactory;

public class GWTApplication extends Application implements EntryPoint {

	@Override
	protected ViewFactory createViewFactory() {

		return new MDLViewFactory(this);
	}

	@Override
	protected ProviderFactory createProviderFactory() {

		return new ProviderFactoryImpl(this);
	}

	@Override
	public void onModuleLoad() {

		GWT.setUncaughtExceptionHandler(t -> showException(t));

		start();
	}

	@Override
	protected Cipher createChiper() {

		return new CryptoJSCipherImpl();
	}

	private void showException(Throwable t) {

		if (getCurrentView() != null) {

			getCurrentView().setEnabled(false);
		}

		String message = "Exception Message:\n\t" + t.getMessage() + "\n\nException Class:\n\t" + t.getClass().getName()
				+ "\n\nStack Trace:\n";

		for (StackTraceElement i : t.getStackTrace()) {

			message += "\t at " + i.getFileName() + ":" + i.getLineNumber() + "\n";
		}

		try {

			getViewFactory().createInfoView().open("Eccezione non gestita", message)
					.whenClose(() -> getProviderFactory().getAuthorizationProvider().get()
							.then(getProviderFactory().getAuthorizationProvider()::revoke)
							.thenAlways(() -> {

								getViewFactory().createLoginView().open();

								return null;
							}));

		} catch (Throwable e) {

			RootPanel.get().clear();
			RootPanel.get().add(new Label(message));
		}
	}
}