package it.fabrizio.gori.claviger.gwt.gapi;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public final class GAPIBasicProfile {

	private GAPIBasicProfile() {

	}

	@JsMethod
	public native String getId();

	@JsMethod
	public native String getName();

	@JsMethod
	public native String getEmail();
}