package it.fabrizio.gori.claviger.gwt.gapi;

import jsinterop.annotations.JsFunction;

@JsFunction
@FunctionalInterface
public interface GAPICallbackFn<T> {

	public void accept(T obj);
}