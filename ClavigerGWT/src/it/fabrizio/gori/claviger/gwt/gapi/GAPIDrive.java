package it.fabrizio.gori.claviger.gwt.gapi;

import elemental2.core.JsArray;
import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public final class GAPIDrive {

	@JsType
	public static final class GetOptions {

		@JsProperty(name = "fileId")
		public native void fileId(String fileId);

		public native void fields(GAPIFile.Fields first, GAPIFile.Fields... others) /*-{

			others.push(first);

			this["fields"] = others.toString();
		}-*/;
	}

	@JsType
	public static final class ListOptions {

		@JsProperty(name = "orderBy")
		public native void orderBy(String orderBy);

		@JsProperty(name = "q")
		public native void searchQuery(String query);

		public native void fields(GAPIFile.Fields first, GAPIFile.Fields... others) /*-{

			others.push(first);

			this["fields"] = "files(" + others.toString() + ")";
		}-*/;
	}

	@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
	public static final class FileSearchResult {

		private FileSearchResult() {

		}

		@JsProperty(name = "files")
		public native <P> GAPIFile<P>[] files();
	}

	private GAPIDrive() {

	}

	@JsMethod(name = "files.get")
	public native <P> GAPIPromise<GAPIResponse<GAPIFile<P>>> get(GetOptions options);

	@JsMethod(name = "files.list")
	public native GAPIPromise<GAPIResponse<FileSearchResult>> list(ListOptions options);

	@JsMethod(name = "files.create")
	private native <P> GAPIPromise<GAPIResponse<GAPIFile<P>>> create(JsPropertyMap<?> options);

	@JsMethod(name = "files.update")
	private native <P> GAPIPromise<GAPIResponse<GAPIFile<P>>> update(JsPropertyMap<?> options);

	@JsMethod(name = "files.delete")
	public native GAPIPromise<GAPIResponse<Void>> delete(String fileId);

	@JsOverlay
	public <P> GAPIPromise<GAPIResponse<GAPIFile<P>>> create(GAPIFile<P> file) {

		JsPropertyMap<Object> opt = JsPropertyMap.of("resource", file);
		JsPropertyMap<Object> filePropertyMap = Js.asPropertyMap(file);
		JsArray<String> fields = new JsArray<String>();

		fields.push(GAPIFile.Fields.ID.toString());

		for (GAPIFile.Fields i : GAPIFile.Fields.values()) {

			if (i != null && filePropertyMap.has(i.toString())) {

				fields.push(i.toString());
			}
		}

		opt.set("fields", fields.join(","));

		return create(opt);
	}

	@JsOverlay
	public <P> GAPIPromise<GAPIResponse<GAPIFile<P>>> update(GAPIFile<P> file) {

		JsPropertyMap<Object> opt = JsPropertyMap.of("fileId", file.getId());
		JsPropertyMap<Object> filePropertyMap = Js.asPropertyMap(file);
		JsArray<String> fields = new JsArray<String>();

		for (GAPIFile.Fields i : GAPIFile.Fields.values()) {

			if (i != null && i.toString() != "id" && filePropertyMap.has(i.toString())) {

				opt.set(i.toString(), filePropertyMap.get(i.toString()));
				fields.push(i.toString());
			}
		}

		opt.set("fields", fields.join(","));

		return update(opt);
	}
}