package it.fabrizio.gori.claviger.gwt.gapi;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.ScriptInjector;

import it.fabrizio.gori.claviger.core.Promise;
import it.fabrizio.gori.claviger.gwt.TimeoutException;
import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public final class GAPI {

	public static final class GAPIInitException extends Exception {

		private static final long serialVersionUID = 1L;

		private final Throwable cause;

		private GAPIInitException(Throwable t) {

			super(t.getMessage());

			cause = t;
		}

		@Override
		public synchronized Throwable getCause() {

			return cause;
		}
	}

	public static final class GAPILoadError extends Error {

		private static final long serialVersionUID = 1L;

		private final Throwable cause;

		private GAPILoadError(Throwable t) {

			super(t.getMessage());

			cause = t;
		}

		@Override
		public synchronized Throwable getCause() {

			return cause;
		}
	}

	@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
	public static final class LoadConfig {

		@JsProperty(name = "timeout")
		public native void setTimeout(int timeout);

		@JsProperty(name = "callback")
		public native void onSuccess(GAPICallbackFn<Void> callback);

		@JsProperty(name = "error")
		public native void onError(GAPICallbackFn<Void> callback);

		@JsProperty(name = "ontimeout")
		public native void onTimeout(GAPICallbackFn<Void> callback);
	}

	@JsProperty(namespace = JsPackage.GLOBAL, name = "gapi")
	private static GAPI instance;

	@JsOverlay
	private static String clientId;
	@JsOverlay
	private static String scope;
	@JsOverlay
	private static String[] discoveryDocs;

	private GAPI() {

	}

	@JsOverlay
	public static void init(String clientId, String scope, String... discoveryDocs) {

		GAPI.clientId = clientId;
		GAPI.scope = scope;
		GAPI.discoveryDocs = discoveryDocs;
	}

	@JsOverlay
	public static Promise<GAPI> get() {

		return new Promise<GAPI>((resolve, reject) -> {

			if (GAPI.scope == null || GAPI.clientId == null || GAPI.discoveryDocs == null || GAPI.discoveryDocs.length <= 0) {

				reject.accept(new Exception("GAPI not initialized correctly"));

			} else if (instance == null) {

				ScriptInjector
						.fromUrl("https://apis.google.com/js/api.js")
						.setWindow(ScriptInjector.TOP_WINDOW)
						.setCallback(new Callback<Void, Exception>() {

							@Override
							public void onSuccess(Void result) {

								resolve.accept(instance);
							}

							@Override
							public void onFailure(Exception reason) {

								reject.accept(reason);
							}
						})
						.inject();

			} else {

				resolve.accept(instance);
			}
		});
	}

	@JsMethod(name = "load")
	private native void load(String libraries, LoadConfig config);

	@JsProperty(name = "client")
	public native GAPIClient getClient();

	@JsMethod(name = "auth2.getAuthInstance")
	public native GAPIAuth getAuthInstance();

	@JsOverlay
	public static Promise<GAPI> load(GAPI gapi) {

		return new Promise<GAPI>((resolve, reject) -> {

			if (gapi.getClient() == null) {

				GAPI.LoadConfig config = new GAPI.LoadConfig();

				config.onError(v -> reject.accept(new Exception("Load error.")));
				config.setTimeout(10000);
				config.onTimeout(v -> reject.accept(new TimeoutException()));
				config.onSuccess(v -> {

					resolve.accept(gapi);
				});

				gapi.load("client:auth2", config);

			} else {

				resolve.accept(gapi);
			}
		});
	}

	@JsOverlay
	public static Promise<GAPI> init(GAPI gapi) {

		return new Promise<GAPI>((resolve, reject) -> {

			if (gapi.getAuthInstance() == null || gapi.getAuthInstance().currentUser() == null || !gapi.getAuthInstance().currentUser()
					.hasGrantedScopes(GAPI.scope)) {

				GAPIClient.InitArgs initConfig = new GAPIClient.InitArgs();

				initConfig.clientId(GAPI.clientId);
				initConfig.scope(GAPI.scope);
				initConfig.discoveryDocs(GAPI.discoveryDocs);

				gapi.getClient().init(initConfig).then(v -> {

					resolve.accept(gapi);

					return null;

				}).thenCatch(err -> {

					reject.accept(new GAPIInitException(GAPIUtil.asError(err)));

					return null;
				});

			} else {

				resolve.accept(gapi);
			}
		});
	}
}
