package it.fabrizio.gori.claviger.gwt.gapi;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public final class GAPIAuth {

	public static enum PromptValues {

		CONSENT("consent"),
		SELECT_ACCOUNT("select_account"),
		NONE("none");

		private final String value;

		private PromptValues(String value) {

			this.value = value;
		}

		@Override
		public String toString() {

			return value;
		}
	}

	@JsType
	public static final class SignInOptions {

		@JsProperty(name = "redirect_uri")
		public native void redirectUri(String uri);

		public native void prompt(PromptValues promptValue) /*-{

			this["prompt"] = promptValue.toString();
		}-*/;
	}

	private GAPIAuth() {

	}

	@JsMethod(name = "isSignedIn.get")
	public native boolean isSignedIn();

	@JsMethod(name = "currentUser.get")
	public native GAPIGoogleUser currentUser();

	@JsMethod(name = "disconnect")
	public native GAPIPromise<Void> disconnect();

	@JsMethod(name = "signIn")
	public native GAPIPromise<GAPIGoogleUser> signIn(SignInOptions options);
}