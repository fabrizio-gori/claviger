package it.fabrizio.gori.claviger.gwt.gapi;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public final class GAPIFile<P> {

	public static enum Fields {

		ID("id"),
		NAME("name"),
		MIME_TYPE("mimeType"),
		PARENTS("parents"),
		APP_PROPERTIES("appProperties");

		private final String fieldName;

		private Fields(String fieldName) {

			this.fieldName = fieldName;
		}

		@Override
		public String toString() {

			return fieldName;
		}
	}

	public static class NotFoundException extends Exception {

		private static final long serialVersionUID = 1L;

		public NotFoundException(String message) {

			super(message);
		}
	}

	@JsProperty(namespace = JsPackage.GLOBAL, name = "gapi")
	private static GAPI gapiInstance;

	public GAPIFile() {

	}

	@JsOverlay
	public GAPI gapi() {

		return gapiInstance;
	}

	@JsProperty
	public native String getId();

	@JsProperty
	public native String getName();

	@JsProperty
	public native void setName(String name);

	@JsProperty
	public native String getMimeType();

	@JsProperty
	public native void setMimeType(String mimeType);

	@JsProperty
	public native String[] getParents();

	@JsProperty
	public native void setParents(String[] parents);

	@JsOverlay
	public void setParent(String parent) {

		setParents(new String[] { parent });
	}

	@JsProperty
	public native P getAppProperties();

	@JsProperty
	public native void setAppProperties(P properties);
}