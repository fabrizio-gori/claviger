package it.fabrizio.gori.claviger.gwt.gapi;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public final class GAPIResponse<T> {

	private GAPIResponse() {

	}

	@JsProperty(name = "status")
	public native int getStatus();

	@JsProperty(name = "result")
	public native T getResult();

	@JsProperty(name = "body")
	public native T getBody();
}
