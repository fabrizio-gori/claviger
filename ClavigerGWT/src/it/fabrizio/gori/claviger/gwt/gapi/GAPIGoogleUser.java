package it.fabrizio.gori.claviger.gwt.gapi;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public final class GAPIGoogleUser {

	private GAPIGoogleUser() {

	}

	@JsMethod
	public native String getId();

	@JsMethod
	public native boolean isSignedIn();

	@JsMethod
	public native String getHostedDomain();

	@JsMethod
	public native String getGrantedScopes();

	@JsMethod
	public native GAPIBasicProfile getBasicProfile();

	@JsMethod
	public native boolean hasGrantedScopes(String scope);
}