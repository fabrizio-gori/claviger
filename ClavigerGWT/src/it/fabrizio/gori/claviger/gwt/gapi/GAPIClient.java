package it.fabrizio.gori.claviger.gwt.gapi;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.JsPropertyMap;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public final class GAPIClient {

	public static enum HTTPMethodValues {

		GET,
		POST,
		PUT,
		DELETE,
		PATCH;
	}

	@JsType
	public static class InitArgs {

		@JsProperty(name = "clientId")
		public native void clientId(String clientId);

		@JsProperty(name = "scope")
		public native void scope(String scope);

		@JsProperty(name = "discoveryDocs")
		public native void discoveryDocs(String[] discoveryDocs);
	}

	@JsType
	public static class RequestOptions {

		@JsProperty(name = "path")
		public native void path(String path);

		public native void method(HTTPMethodValues method) /*-{

			this["method"] = method.toString();
		}-*/;

		@JsProperty(name = "params")
		public native void params(JsPropertyMap<?> params);

		@JsProperty(name = "headers")
		public native void headers(Object headers);

		@JsProperty(name = "body")
		public native void body(Object body);
	}

	private GAPIClient() {

	}

	@JsMethod(name = "init")
	public native GAPIPromise<Void> init(InitArgs args);

	@JsProperty(name = "drive")
	public native GAPIDrive getDrive();

	@JsMethod(name = "request")
	public native <T> GAPIPromise<GAPIResponse<T>> request(RequestOptions options);
}