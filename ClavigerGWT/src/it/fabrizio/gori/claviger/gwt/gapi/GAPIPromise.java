package it.fabrizio.gori.claviger.gwt.gapi;

import elemental2.promise.Promise.CatchOnRejectedCallbackFn;
import jsinterop.annotations.JsFunction;
import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL)
public final class GAPIPromise<T> {

	@JsFunction
	public static interface ThenOnFulfilledCallbackFn<T, V> {

		GAPIPromise<T> onInvoke(T p0);
	}

	@JsFunction
	public static interface ThenOnRejectedCallbackFn<V> {

		GAPIPromise<V> onInvoke(Object p0);
	}

	@JsFunction
	public static interface AlwaysCallbackFn {

		void onInvoke();
	}

	public native <V> GAPIPromise<V> then(ThenOnFulfilledCallbackFn<? super T, ? extends V> onFulfilled);

	@JsMethod(name = "catch")
	public native <V> GAPIPromise<V> thenCatch(CatchOnRejectedCallbackFn<? extends V> onRejected);

	@JsOverlay
	public final GAPIPromise<T> thenAlways(AlwaysCallbackFn onAlways) {

		return then(r -> {

			onAlways.onInvoke();

			return this;

		}, err -> {

			onAlways.onInvoke();

			return this;
		});
	}

	private native <V> GAPIPromise<V> then(
			ThenOnFulfilledCallbackFn<? super T, ? extends V> onFulfilled,
			ThenOnRejectedCallbackFn<? extends V> onRejected);
}