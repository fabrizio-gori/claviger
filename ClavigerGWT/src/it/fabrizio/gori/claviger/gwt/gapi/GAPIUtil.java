package it.fabrizio.gori.claviger.gwt.gapi;

public final class GAPIUtil {

	public static native String asString(Object o) /*-{

		try {

			if (typeof o === "string") {

				return o;

			} else {

				var result = "";

				if (typeof o.message === "string") {

					result += o.message + "\n";
				}

				if (typeof o.error === "string") {

					result += o.error + "\n";
				}

				if (typeof o.details === "string") {

					result += o.details + "\n";
				}

				if (o.error && typeof o.error.message === "string") {

					result += o.error.message + "\n";
				}

				if (o.error && o.error.errors && Array.isArray(o.error.errors)
						&& o.error.errors.length > 0) {

					for (var i = 0; i < o.error.errors.length; i++) {

						for ( var j in o.error.errors[i]) {

							if (typeof o.error.errors[i][j] === "string") {

								result += "\t" + j + ": "
										+ o.error.errors[i][j] + "\n";
							}
						}

						result += "\n";
					}

					result += "\n";
				}

				if (o.result && o.result.error
						&& typeof o.result.error.message === "string") {

					result += o.result.error.message + "\n";
				}

				if (o.result && o.result.error && o.result.error.errors
						&& Array.isArray(o.result.error.errors)
						&& o.result.error.errors.length > 0) {

					for (var i = 0; i < o.result.error.errors.length; i++) {

						for ( var j in o.result.error.errors[i]) {

							if (typeof o.result.error.errors[i][j] === "string") {

								result += "\t" + j + ": "
										+ o.result.error.errors[i][j] + "\n";
							}
						}

						result += "\n";
					}

					result += "\n";
				}

				return result.slice(0, -1);
			}

		} catch (e) {

			return "Generic error";
		}
	}-*/;

	public static Error asError(Object o) {

		return new Error(asString(o));
	}
}
