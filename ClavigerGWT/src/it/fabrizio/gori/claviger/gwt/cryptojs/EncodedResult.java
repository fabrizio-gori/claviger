package it.fabrizio.gori.claviger.gwt.cryptojs;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL)
interface EncodedResult {

	@JsMethod(name = "toString")
	public String getText();

	@JsMethod(name = "toString")
	public String getText(Object Encoder);
}