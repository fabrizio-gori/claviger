package it.fabrizio.gori.claviger.gwt.cryptojs;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL)
class CryptoJS {

	@JsMethod(name = "MD5")
	public static native EncodedResult hashMD5(String message);

	@JsMethod(name = "SHA1")
	public static native EncodedResult hashSHA1(String message);

	@JsProperty(name = "TripleDES")
	public static native TripleDES getTripleDes();

	@JsProperty(name = "AES")
	public static native AES getAES();
}