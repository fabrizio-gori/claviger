package it.fabrizio.gori.claviger.gwt.cryptojs;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = "CryptoJS", name = "enc")
class Encoder {

	@JsProperty(name = "Utf8")
	public static native Object getUtf8();
}