package it.fabrizio.gori.claviger.gwt.cryptojs;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

import it.fabrizio.gori.claviger.core.Cipher;

public final class CryptoJSCipherImpl implements Cipher {

	public static interface Resources extends ClientBundle {

		final Resources INSTANCE = GWT.create(Resources.class);

		@Source("resources/aes.js")
		TextResource jsAES();

		@Source("resources/tripledes.js")
		TextResource jsTripledes();

		@Source("resources/sha1.js")
		TextResource jsSHA1();
	}

	static {

		ScriptInjector.fromString(CryptoJSCipherImpl.Resources.INSTANCE.jsSHA1().getText())
				.setWindow(ScriptInjector.TOP_WINDOW).inject();

		ScriptInjector.fromString(CryptoJSCipherImpl.Resources.INSTANCE.jsAES().getText())
				.setWindow(ScriptInjector.TOP_WINDOW).inject();
	}

	private final AES aes = CryptoJS.getAES();

	@Override
	public String encrypt(String value, String password) {

		return aes.encrypt(value, password).getText();
	}

	@Override
	public String decrypt(String value, String password) {

		return aes.decrypt(value, password).getText(Encoder.getUtf8());
	}

	@Override
	public String hash(String value) {

		return CryptoJS.hashSHA1(value).getText();
	}
}