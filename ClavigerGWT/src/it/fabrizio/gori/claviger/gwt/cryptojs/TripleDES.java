package it.fabrizio.gori.claviger.gwt.cryptojs;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL)
interface TripleDES {

	@JsMethod
	public EncodedResult encrypt(String message, String password);

	@JsMethod
	public EncodedResult decrypt(String message, String password);
}
