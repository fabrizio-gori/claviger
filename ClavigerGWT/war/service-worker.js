importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.6.2/workbox-sw.js');

if(workbox) {
	
	workbox.setConfig({ debug: false });
	
	workbox.precaching.precacheAndRoute([
		/*TODO: precaricare file ./gwt/*.js generati in compiazione*/
		'./gwt/gwt.nocache.js',
		'./gwt/clear.cache.gif',
		'./mdl/material.min.EM.js',
		'./mdl/material.green-light_green.min.EM.css',
		'./workers/idb-worker.js',
		'./index.html',
		'./debug.html',
		'./favicon.png',
	]);
 
	workbox.routing.registerRoute(
		/.*(?:googleapis)\.com.*$/,
		workbox.strategies.staleWhileRevalidate({
		cacheName: 'googleapis',
		}),
	);

	workbox.routing.registerRoute(
		/.*(?:gstatic)\.com.*$/,
		workbox.strategies.staleWhileRevalidate({
		cacheName: 'gstatic',
		}),
	);
	
} else {
	
	console.log("Workbox didn't load.");
}