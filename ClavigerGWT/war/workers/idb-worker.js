var dbInstances = {};

var getIDB = function(idbDefinition, onSuccess, onError) {
	
	if(!dbInstances[idbDefinition.dbName]) {
	
		if (!("indexedDB" in self)) {
			
			onError("IndexedDB not supported.");
			
		} else {
		
			var request = indexedDB.open(idbDefinition.dbName, idbDefinition.dbVer);
			
			request.onerror = function(evt) {

				onError("Unable to open IndexDB");
			};

			request.onsuccess = function(evt) {
				
				try {
				
					var db = request.result;
					
					db.onclose = function(evt1) {
	
						delete dbInstances[idbDefinition.dbName];
					};
					
					dbInstances[idbDefinition.dbName] = db;
	
					onSuccess(db);
					
				} catch(err) {

					onError(err);
				}
			};

			request.onupgradeneeded = function(evt) {

				try {

					var db = request.result;

					if (db.objectStoreNames.contains(idbDefinition.objectStoreName)) {

						db.deleteObjectStore(idbDefinition.objectStoreName);
					}

					db.createObjectStore(idbDefinition.objectStoreName, { keyPath: idbDefinition.keyPath });

				} catch(err) {

					onError(err);
				}
			};
		}
		
	} else {
	
		onSuccess(dbInstances[idbDefinition.dbName]);
	}
};

var getElement = function(idbDefinition, username, onSuccess, onError) {

	getIDB(idbDefinition, function(db) {
		
		try {
		
			var transaction = db.transaction(idbDefinition.objectStoreName, "readonly");
			var objectStore = transaction.objectStore(idbDefinition.objectStoreName);
			var request = objectStore.get(username);

			request.onsuccess = function(evt) {

				onSuccess(request.result);
			};

			request.onerror = function(evt) {

				onError("Unable to get element");
			};
			
		} catch(err) {
			
			onError(err);
		}
		
	}, onError);
};

var deleteElement = function(idbDefinition, username, onSuccess, onError) {

	getIDB(idbDefinition, function(db) {
		
		try {
		
			var transaction = db.transaction(idbDefinition.objectStoreName, "readwrite");
			var objectStore = transaction.objectStore(idbDefinition.objectStoreName);
			var request = objectStore.delete(username);

			request.onsuccess = function(evt) {

				onSuccess();
			};

			request.onerror = function(evt) {

				onError("Unable to delete element");
			};
			
		} catch(err) {
			
			onError(err);
		}
		
	}, onError);
};

var putElement = function(idbDefinition, obj, onSuccess, onError) {

	getIDB(idbDefinition, function(db) {
		
		try {
		
			var transaction = db.transaction(idbDefinition.objectStoreName, "readwrite");
			var objectStore = transaction.objectStore(idbDefinition.objectStoreName);
			var request = objectStore.put(obj);

			request.onsuccess = function(evt) {

				onSuccess(obj);
			};

			request.onerror = function(evt) {

				onError("Unable to put element");
			};
			
		} catch(err) {
			
			onError(err);
		}
		
	}, onError);
};

self.onmessage = function(msgEvt) {

	var opt = msgEvt.data;
	
	switch(opt.operation) {
		
		case "GET":
			
			getElement(opt.idbDefinition, opt.param, function(result) {
			
				self.postMessage(result);
			
			}, function(err) {
				
				throw err;
			});
			
			break;
			
		case "PUT":
			
			putElement(opt.idbDefinition, opt.param, function(result) {
			
				self.postMessage(result);
			
			}, function(err) {
				
				throw err;
			});
			
			break;
		
		case "DELETE":
			
			deleteElement(opt.idbDefinition, opt.param, function(result) {
			
				self.postMessage(result);
			
			}, function(err) {
				
				throw err;
			});
			
			break;
						
		default:
			throw "Unknow operation " + opt.operation
	}
}